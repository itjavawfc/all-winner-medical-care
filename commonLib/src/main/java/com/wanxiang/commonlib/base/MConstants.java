package com.wanxiang.commonlib.base;

import com.wanxiang.commonlib.utils.ICommonContext;

public interface MConstants {

    String KEY_CODE_NUMBER = "stripCode"; //产品编码

    String DELING_SERVER = "deling_server";  //服务器接口、mqtt日志

    String SCREEN_MODEL = "screenmodel";

    //String SYNC_GROUP_ID = "sync_groupid";

    String SCREEN_MODEL_CLOCK_PAPWER_TIME = "clock_paper_time";  //clokc  Paper 需要自己记录时间  单位miao
    /**
     * 锁屏息屏
     */
    int DISPLAY_SCREEN_XIPING = 0;
    /**
     * 锁屏时钟
     */
    int DISPLAY_SCREEN_CLOCK = 1;
    /**
     * 锁屏杂志
     */
    int DISPLAY_SCREEN_PAPER = 2;

    String SCREEN_MODEL_MAGAZINE = "screenmodel_magazine";

    String UP_SUCCESS_IS_VOICE = "up_success_is_voice";

    /**
     * 锁屏杂志中，选中的是本地图片
     */
    int SCREEN_MODEL_MAGAZINE_LOCALPIC = 0;
    /**
     * 锁屏杂志中，选中的是华尔思杂志
     */
    int SCREEN_MODEL_MAGAZINE_MG = 1;
    String DISTURB_MODEL = "nodisturb_model";         //勿扰模式
    String DISTURB_TIME = "nodisturb_time";         //勿扰模式时间
    int DISTURB_MODEL_OPEN = 0;                 //勿扰模式  开
    int DISTURB_MODEL_CLOSE = 1;               //勿扰模式   关闭
    String LANGUE_MODEL = "langue_model";
    int LANGUE_CHINESE = 0;
    int LANGUE_ENGLISH = 1;
    String TEXT_SIZE_MODEL = "text_size_model";
    int TEXT_SIZE_MIDDLE = 0;
    String BIND_DEVICE_MASTERID = "bind_device_masterId";       // 绑定机器的masterId
    String ARIA_DOWNLOAD = "aria_download";
    /**
     * 提示用户接下来的操作,让用户可选择是否继续
     */
    String HINT_EXECUTE = "hint_execute";
    /**
     * 标记常量  类似于true--false用途
     */
    String OK_AFFIRM = "ok_affirm";
    String NO_CANCEL = "no_cancel";

    /**
     * 未知状态
     */
    int ARIA_UNKNOWN = -1; //-1	未知状态
    /**
     * 失败
     */
    int ARIA_FAILURE = 0;//0	失败
    /**
     * 成功
     */
    int ARIA_SUCCESS = 1;//1	成功
    /**
     * 停止
     */
    int ARIA_STOP = 2;//2	停止
    /**
     * 等待
     */
    int ARIA_WAIT = 3;//3	等待
    /**
     * 执行中
     */
    int ARIA_IN_EXECUTION = 4;//4	执行中
    /**
     * 预处理
     */
    int ARIA_PRETREATMENT = 5;//5	预处理
    /**
     * 预处理完成
     */
    int ARIA_PRETREATMENT_ACCOMPLISH = 6;//6	预处理完成
    /**
     * 删除任务
     */
    int ARIA_DELETE_TASK = 7;//7	删除任务
    /**
     * 桌面是否安装成功
     */
    String LAUNCHER_INSTALL_IS_SUCCESS = "launcher_install_is_success";
    /**
     * 跳过当前新版本的更新
     */
    String SKIP_APP_VERSION_HINT = "skip_app_version_hint";
    /**
     * 跳过提示的版本号
     */
    String SKIP_LAUNCHER_VERSION_CODE = "skip_launcher_version_code";
    String SKIP_VOICE_VERSION_CODE = "skip_voice_version_code";
    String SKIP_TOOLS_VERSION_CODE = "skip_tools_version_code";
    /**
     * 查询到的apk版本code
     */
    String NEWEST_LAUNCHER_VERSION_CODE = "newest_launcher_version_code";
    String NEWEST_VOICE_VERSION_CODE = "newest_voice_version_code";
    String NEWEST_TOOLS_VERSION_CODE = "newest_tools_version_code";

    String LAUNCHER_PACK_NAME = "com.deling.launcher";
    String VOICE_PACK_NAME = "com.deling.voice";
    String TOOLS_PACK_NAME = "com.hrs.tools";


    String UP_SUCCESS_HINT_WINDOW = "up_success_hint_window";

    String IS_SHOW_MARK_IMG = "is_show_mark_img";

    /**
     * 38368287对应九宫格拼音eventbus
     * 涂鸦音乐控制常量
     */
    String EVENT_UP_DGNZK_VOLUME = "38368287_101_volume";               //  播放暂停切换                  dp : 101
    String EVENT_UP_DGNZK_RESUME_MUSIC = "38368287_102_play";           //  播放暂停切换                  dp : 102
    String EVENT_UP_DGNZK_PAUSE_MUSIC = "38368287_102_pause";           //  播放暂停切换                  dp : 102
    String EVENT_UP_DGNZK_UP_DOWN_MUSIC = "38368287_103";               //  切换上下曲                    dp : 103
    String EVENT_UP_DGNZK_PLAY_MODE = "38368287_106";                   //  播放模式                      dp : 106
    String EVENT_UP_DGNZK_LINKAGE_ZONE = "38368287_108";                //  联动切换                      dp : 108
    String EVENT_UP_DGNZK_MUSIC_LIST = "38368287_109";                  //  上报歌曲列表                  dp : 109
    String EVENT_UP_DGNZK_SWITCH_SUBAREA = "38368287_113";              //  分区切换                      dp : 113      par_one  par_two
    String EVENT_UP_DGNZK_AUDIO_SOURCE = "38368287_114";                //  音源选择                      dp : 114      "local"  "blue"  "aux"   "kugou"
    String EVENT_UP_DGNZK_SOUND_EFFECT = "38368287_115";                //  音效选择                      dp : 115       "local"  "blue"  "aux"   "kugou"

    String ACTION_FACTORY_RESET = "factory_reset";

    String SHEET_MENU_PAGE = "1";         // 歌单Page 页面 用于歌单一级菜单 获取二级菜单1页只需要三个数据
    String SHEET_MENU_PAGE_SIZE = "3";    // 歌单Page 页面 用于歌单一级菜单 获取二级菜单1页只需要三个数据



    String WAKE_CUSTOM_NAME = "wake_custom_name";   //自定义唤醒词名称
    String WAKE_CUSTOM_NAME_PINYIN = "wake_custom_name_pinyin";
    String VOICE_SENSITIVITY = "voice_sensitivity";         //灵敏度模式
    int VOICE_SENSITIVITY_RECOMMAND = 0;              //灵敏度 推荐
    int VOICE_SENSITIVITY_NORMAL = 1;               //灵敏度  普通
    int VOICE_SENSITIVITY_VOICE_SENSITIVE = 2;               //灵敏度  灵敏

    String WAKE_MODEL = "wake_model";               //唤醒词状态

    int WAKE_MODEL_DEFAULE = 0;
    int WAKE_MODEL_CUSTOM = 1;

    String PAD_INSERT_OUT_STATE = "pad_insert_out_state";
    String PAD_VOICE_STATE = "pad_voice_state";

    String DJ_LIGHT_COLOR_MARGIN ="DJ_LIGHT_COLOR_MARGIN" ;   //颜色
    String DJ_LIGHT_TMP_MARGIN ="DJ_LIGHT_TMP_MARGIN" ;       // 色温

    String MUSIC_INFO_TUIJIAN ="MUSIC_INFO_TUIJIAN" ;       // 歌单推荐

    String SCREEN_WEATHER_DATA = "screen_weather_data";


    /**
     * 息屏时间 30s
     */
    int DISPLAY_TIME_THIRTY_SECONDS = 30;
    /**
     * 息屏时间 1m
     */
    int DISPLAY_TIME_ONE_MINUTES = 60;
    /**
     * 息屏时间 5m
     */
    int DISPLAY_TIME_FIVE_MINUTES = 5 * 60;
    /**
     * 息屏时间 10m
     */
    int DISPLAY_TIME_TEN_MINUTES = 10 * 60;
    /**
     * 息屏时间 30m
     */
    int DISPLAY_TIME_THIRTY_MINUTES = 30 * 60;
    /**
     * 息屏时间 永不
     */
    int DISPLAY_TIME_NEVRY_TIME = Integer.MAX_VALUE / 1000;

     int TEXT_SIZE_SMALL = 1;
     int TEXT_SIZE_LARGE = 2;
    int TEXT_SIZE_HLARGE = 3;

    String VOICE_MODEL = "voice_model";         //语音模式
    int VOICE_MODEL_RECOMMAND = 0;              //语音推荐
    int VOICE_MODEL_LINK = 1;               //语音连续对话

    String VOICE_SWITCH = "voice_switch";         //唤醒方式
    int VOICE_SWITCH_DEFAULT = 0;              //默认
    int VOICE_SWITCH_AWAKEN = 1;               //直面唤醒

    String DISTURB_MODEL_CLOCK_IDS = "nodisturb_model_ids";

    /**
     * 进入勿扰模式
     */
    int DISTURB_MODEL_START = 2;
    /**
     * 退出勿扰模式
     */
    int DISTURB_MODEL_EXIT = 3;
    int BG_SELECT_LOCAL = 0;               //锁屏杂志类型：本地图片
    int BG_SELECT_MAGAZINE = 1;             //锁屏杂志类型：杂志图片
    String BG_SELECT_LOCAL_INDEX = "screen_localpic_index";               //锁屏杂志类型：本地图片  选中index
    String BG_SELECT_MAGAZINE_INDEX = "screen_magzine_index";             //锁屏杂志类型：杂志图片 选中index

    String SYNC_GROUPID = "sync_groupid";             //同步的groupid
    /**
     * 是否同步,初始化注册设备到云服务器
     */
    String IS_REGISTER_DEVICE = "isregisterdevcie";

    String IS_WIFI = "iswifi";//当前是wifi网络

     String IS_DISPLAY_FRAGMENT = "display_fragment";       // 是否是显示Fragment item
    String IS_DISPLAY_MORE_WIFI = "display_more_wifi";       // 是否是显示Fragment item

    String PUBLISH_PRO_TOPIC = "deling_receive_pro_";
    String IS_SPEAK_FRAGMENT = "display_fragment";

    String IS_JDQ_DEVICE = "isjdqdevice";//继电器是否在底座上

    /**
     * 重启定时是否存在
     */
    String REBOOT_CLOCK_IS_EXIST = "reboot_clock_is_exist";

    String ELECT = "elect";
    /**
     * 编辑定时
     */
    int EDIT_CLOCK = 1;
    /**
     * 添加定时
     */
    int ADD_CLOCK = 0;


    /**
     * 删除组网
     */
    String DELETE_GROUP = "delete_group";
    /**
     * 云分享开关
     */
    String CLOUD_SHARE_SWITCH = "cloud_share_switch";
    /**
     * 云分享模式
     */
    String CLOUD_SHARE_PATTERN = "cloud_share_pattern";
    /**
     * 云分享关
     */
    int CLOUD_SHARE_CLOSE = 0;
    /**
     * 云分享开
     */
    int CLOUD_SHARE_OPEN = 1;
    /**
     * 云分享自动接收
     */
    int CLOUD_SHARE_AUTO = 2;
    /**
     * 云分享手动接收
     */
    int CLOUD_SHARE_HAND = 3;
    /**
     * 定时播放
     */
    int CLOCK_PLAY = 0;
    /**
     * 定时停止
     */
    int CLOCK_STOP = 1;
    /**
     * 定时重启
     */
    int CLOCK_REBOOT = 2;
    /**
     * 删除数据
     */
    String DELETE_DATA = "delete_DATA";





    String LOCAL_MUSIC = "local_music";

    String PLAY_LIST_MUSIC = "play_list_music";

    int START = 1;
    int STOP = 0;

    int ALLZONE = 100;

    float ALPHA_0_5 = 0.5f;
    String   INTERCOM_RECEPTION= "INTERCOMRECEPTION";                //对讲接收声音 SP
    String   SHEET_FIRST_SIX_MENU= "sheet_first_six_level_menu";       //Sheet歌单一级目录6个菜单选项存储缓存数据   本地三天缓存
    String   SHEET_CATEGORY_MENU= "sheet_CATEGORY_MENU";             //Sheet歌单 一级目录  本地三天缓存
    String   SHEET_MENU_SONG= "sheet_menu_song";                     //Sheet歌单一级目录下二级目录的三个拿出来的数据
    String   SHEET_QUICT_TO_MUSICLIST= "sheet_quick_to_musiclist";                     //sheet 歌单，一级界面直接进入歌曲列表的Falg
    String   RANK_FIRST_LEVEL_MENU= "rank_first_level_menu";                     //排行榜，一级菜单缓存
    String   RADIO_FIRST_LEVEL_MENU= "radio_first_level_menu";                     //音乐电台一级二级菜单
    String   INTERCOM_ALARM= "INTERCOMRECMALARM";//闹钟的SP
    String MSELECT_FAMILY_ID_SP = "MSELECT_FAMILY_ID_SP";               //选中的家庭ID
    String MSELECT_FAMILY_NAME_SP ="MSELECT_FAMILY_NAME_SP";            //选中的家庭名称
    String MSELECT_ROOM_SP = "MSELECT_ROOM_SP";                         //选中的房间ID
    String MSELECT_ROOMNAME_SP = "MSELECT_ROOMNAME_SP";                         //选中的房间名称
    String ROOMS_SP = "ROOMS_SP";                         //房间的集合
    String DEVICES_SP = "DEVICES_SP";                     //设备的集合
    String HOME_LISTS = "homelists";            //存储一份家庭列表在本地
    String GATEWAY_READY = "gateway_ready";     // 判断网关是否已经准备好,如果没有准备好不允许进入: ready{1:网关成功拉取到了数据; 2:网关启动成功后,但是调用数据阶段出错了，也视为准备好了，比如接口报错了}

    String MSELECT_DEVICE_MENU_SP = "MSELECT_DEVICE_MENU_SP";                         //选中的index   首页device,选中menu【全部、电灯、开关、插座、窗帘、空调】
    String CACHE_DEVICE_LIST = "CACHE_DEVICE_LIST";
    String DEVICE_ZHIDING_LIST = "DEVICE_ZHIDING_LIST";
    String DEVICE_KUAIJIE_LIST = "DEVICE_KUAIJIE_LIST";                               //快捷产品
    String SCENE_KUIJIE_LIST = "SCENE_KUIJIE_LIST";
    String GATTWAY_HOMEID = "gattwayhomeid";
    String NOW_SCENE_LIST = "now_scene_list";               //存储当前场景列表
    String NOW_DEVICE_LIST= "now_device_list";              //当前设备列表

    int  HOME_DEVICE_ALL=0;
    int  HOME_DEVICE_LED=1;
    int  HOME_DEVICE_SWITCH=2;
    int  HOME_DEVICE_SOCKET=3;
    int  HOME_DEVICE_CURTAINS=4;
    int  HOME_DEVICE_AIRCONDITIONER=5;
    int  HOME_DEVICE_OTHER=6;

    String MORE_ONE = "QQ音乐";//首页更多应用一

    /**
     * GatewayService
     */
    String GET_AUTH_MSG_UUID = "uuid";
    String GET_AUTH_MSG_AUTHKEY = "authKey";
    String SKIP_WIFI_CONNECT = "skip_wifi_connect";       // 跳过WIFI连接
    String SKIP_QR_TUYA = "SKIP_QR_TUYA";                 // 跳过涂鸦二维码扫描
    String RESTE_GATE_WAY = "RESET_GATE_WAY";                 // 预防涂鸦网关重置弹出登录二维码
    String NetworkErrorCode="103";
    String REMOTE_API_RUN_UNKNOW_FAILED="REMOTE_API_RUN_UNKNOW_FAILED";
    String NetWorkErrorCodeTuYa="108";
    String TuYaNetWorkErrorCode="102";
    int SCREEN_WIDTH_1024=1024;   // 标准适配是1280*800,单独对1024 *600 机器进行适配

    String TUYA_LOGIN_TIP="tuya_login_tip";
    String TUYA_VOICE_SPEECH="voice_speech";
    String TUYA_HOME_SELECTED="tuya_home_select_0";

    String CENTER_DIALOG_OPEN="CENTER_DIALOG_OPEN";
    String CENTER_DIALOG_OPEN_TRUE="0";
    String CENTER_DIALOG_OPEN_FALSE="1";
    String CENTER_DIALOG_TOTAL_VOICE="CENTER_DIALOG_TOTAL_VOICE";
    String CENTER_DIALOG_TOTALVOLICE_SET="0";   //0 代表 进度条拖动设置，flag:防止系统音量调整，广播发送，再次设置引起的第三方进度偏差


    String GATEWAY_STATE_KEY="GATEWAY_STATE_KEY";  //当前网关授权状态
    int GATEWAY_STATE_CALLBACKING=0;                  //当前网关正在启动
    int GATEWAY_STATE_START_SUCCESS=1;                //当前网关启动成功
    int GATEWAY_STATE_START_FAILED=2;                 //当前网关启动失败
    int GATEWAY_STATE_PARAM_ERROR=3;                 //授权参数不对，请检查一下SN 是否正确、或者联系服务商


    String DISTURB_MODEL_CLOCK_IDS_KG = "nodisturb_model_ids_kg";
    String DISTURB_MODEL_KG = "nodisturb_model_kg";         //K歌模式
    String DISTURB_TIME_KG = "nodisturb_time_kg";         //K歌模式时间
    String IS_KG = "iskg";         //K歌模式是否打开
    int DISTURB_MODEL_OPEN_KG = 0;                 //K歌节能模式  开
    int DISTURB_MODEL_CLOSE_KG = 1;               //K歌节能模式   关闭

    int OPEN_KG_STATE = 0;                 //K歌模式  开
    int CLOSE_KG_STATE = 1;               //K歌模式   关闭


    /**
     * 进入勿扰模式
     */
    int DISTURB_MODEL_START_KG = 2;
    /**
     * 退出勿扰模式
     */
    int DISTURB_MODEL_EXIT_KG = 3;

    long CLICK_COMMON_DELAY=600;
    long CLICK_SLOW_DELAY=1200;
    long CLICK_SLOW_VIP=1800;
    long CLICK_HINTS_COUNT_TIME=1000;
    long CLICK_HINTS_VIP_DIALOG_TIME=6000;


    String KNOB_TYPE = "knobtype";//旋钮选择类型0音乐 1灯具 2窗帘 3空调
    int KNOB_TYPE_MUSIC = 0;
    int KNOB_TYPE_DJ = 1;
    int KNOB_TYPE_CL = 2;
    int KNOB_TYPE_KT = 3;

    String KNOB_DEVICE ="knob_device";//旋钮选择保存在本地的设备

    String KNOB_IS_USE = "knob_is_Use"; //旋钮是否可用
    int KNOB_IS_ON = 0;
    int KNOB_IS_OFF = 1;

    String KNOB_TOUCH_TYPE_ONE = "knob_touche_type_one"; //触摸控制一保存在本地的类型,0是开关设备,1是场景
    int KNOB_SWITCH_DEVICE_ONE = 0;
    int KNOB_SCREEN_ONE = 1;
    String KNOB_TOUCH_BEAN_ONE = "knob_touch_bean_one"; //触摸控制一保存在本地的设备包括场景

    String KNOB_TOUCH_TYPE_TWO = "knob_touche_type_two"; //触摸控制二保存在本地的类型,0是开关设备,1是场景
    int KNOB_SWITCH_DEVICE_TWO = 0;
    int KNOB_SCREEN_TWO = 1;
    String KNOB_TOUCH_BEAN_TWO = "knob_touch_bean_two"; //触摸控制二保存在本地的设备包括场景

    String GESTURE_STONE_TYPE = "gesture_stone";//石头手势保存在本地的类型，0是开关设备，1是场景
    int GESTURE_STONE_DEVICE = 0;
    int GESTURE_STONE_SCREEN = 1;
    String GESTURE_STONE_BEAN = "gesture_stone_bean";//石头手势保存在本地的设备包括场景

    String GESTURE_SCISSORS_TYPE = "gesture_scissors";//剪刀手势保存在本地的类型，0是开关设备，1是场景
    int GESTURE_SCISSORS_DEVICE = 0;
    int GESTURE_SCISSORS_SCREEN = 1;
    String GESTURE_SCISSORS_BEAN = "gesture_scissors_bean";//剪刀手势保存在本地的设备包括场景

    String GESTURE_CLOTH_TYPE = "gesture_cloth";//布手势保存在本地的类型，0是开关设备，1是场景
    int GESTURE_CLOTH_DEVICE = 0;
    int GESTURE_CLOTH_SCREEN = 1;
    String GESTURE_CLOTH_BEAN = "gesture_cloth_bean";//布手势保存在本地的设备包括场景

    int TOUCHTYPE_ONE = 0; //触摸控制一
    int TOUCHTYPE_TWO = 1; //触摸控制二
    int GESTURE_STONE_TOUCH = 3;//石头手势
    int GESTURE_SCISSORS_TOUCH = 4;//剪刀手势
    int GESTURE_CLOTH_TOUCH = 5;//布手势

    String MAIN_BACKGROUND_INDEX = "main_background_index";
    String SP_SCENE_MUSIC_SELECT_INDEX = "SP_SCENE_MUSIC_SELECT_INDEX";
    String PACKAGE_VOICE_SPEAK="com.voicespeak";

    String SP_MAIN_SCENE_ONE_INDEX = "SP_MAIN_SCENE_ONE_INDEX";
    String SP_MAIN_SCENE_TWO_INDEX = "SP_MAIN_SCENE_TWO_INDEX";
    String SP_IS_JUMP_APP = "ISJumpApp";//判断是否调到外部应用(爱奇艺跟QQ音乐)
    String SP_MIJIA_LOGIN_STATE = "MIJIA_LOGIN_STATE";
    String SP_ISREFRESH_FRAGMENT = "isRefreshFragment";

    String VOLUME_SYS_VALUE ="VOLUME_SYS_VALUE";        //记录当前总音量值
    String VOLUME_ONEAREA_VALUE = "VOLUME_ONEAREA_VALUE";        //记录当前一分区值
    String VOLUME_TWOAREA_VALUE = "VOLUME_TWOAREA_VALUE";        //记录当前二分区值
    String SELECT_ITEM_INDEX = "SelectorItem";//记录当前选中的Item的index
    String SELECT_ITEM_PACKAGESName = "SelectItemPackagesName";//记录当前选中的Item的PackagesName



    String SCREEN_IS_OPEN ="SCREEN_IS_OPEN";        //记录当前是否是屏保界面
    int SCREEN_STATE_CLOSE = 0;                 //非屏保界面
    int SCREEN_STATE_OPEN = 1;               //屏保界面

    String EDIT_SUCCESS_SCENE = "edit_success_scene";   //场景执行成功
    String EDIT_FAIL_SCENE = "edit_success_fail";   //场景执行失败
    String QUNZU_NAME_LIST= "qunzu_name_list"; //定义一个所有群组的集合

    String IS_REBOOT = "is_reboot";

      int NET_NOCONNECT = 0;
      int NET_ETHERNET = 1;
      int NET_WIFI = 2;
    String MIGU_ID_NOTAPPLICATE="999029";   //未导入CODE
    String MIGU_ID_NOTAPPLICATE_CODE="90000";   //未导入CODE
     String MIOT_SUCCESS = "miotInit_Success";
    String MIOT_IS_DESTORY = "miotisDestory";
    String LOCAL_SWITCH_ONE = "local_switch_one";
    String LOCAL_SWITCH_TWO = "local_switch_two";
    String HISTORY_MUSIC = "history_music";
    String HISTORY_MUSIC_ONE = "history_music_one";
    String HISTORY_MUSIC_TWO = "history_music_two";

    int DEV_REFRESH = 0;

    String IS_GW_SUCCESS = "is_gw_success";
    String KNOBCONTROLDIALOG_VOLUME = "KnobControlDialog_Volume";
    String GW_DEV_ID = "gw_dev_id";
    String VOICE_VOCALS = "voice_vocals";

    String TEST_TUYA_DEVICE = "test_tuya_dev";
    String GET_TUYA_TOKEN_TIME = "GET_TUYA_TOKEN_TIME";
    String KNOB_KEY_CONTROL_TIME = "KNOB_KEY_CONTROL_TIME";
    String TUYA_PAY_SUCCESS = "TUYA_PAY_SUCCESS";
    String NOW_IOT_TYPE = "NOW_IOT_TYPE";
    //当前IOT的类型:
    //triple_tuya
    //triple_mijia
    //triple_sbc
    String NOW_VOICE_TYPE = "NOW_VOICE_TYPE";
    //当前语音的人声类型
    //0 甜美女声
    //1 鬼故事(主播)
    //2 阳光男孩
    //3 男播音员
    //4 女教师
    //5 帅气男童
    //6 可爱女童
    //7 商务女声
    //8 唯美电台
    String CHANGETUYA_HINT = "CHANGETUYA_HINT";
}
