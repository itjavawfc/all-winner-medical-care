package com.wanxiang.commonlib.base;

import android.graphics.drawable.Drawable;

/**
 * Created by Changer on 2021/5/14
 */
public interface ChannelCustom {
    String getBottomAppEnumName(); //获取底部枚举的第三个名称  ,天乔的要客户自己的  ,wise显示智能家居
    String getBottomAppEnumAppName(); //获取底部枚举的第三个名称  ,天乔的要客户自己的  ,wise显示智能家居
    Drawable getBottomAppEnumIcon();
    String getBottomAppEnumPackageName();
    String productModel();//是否显示产品型号
    String firmwareHint();//当前固件版本为
    String newFirmwareHint();//新固件
    String model();//型号  wise 和music
    String getDeviceName();   //设备名称
    int    getVoiceMicType(); //获取mic 类型
    String getAppkey(); //获取appKey
    String getAppSerect(); //获取appserect

}
