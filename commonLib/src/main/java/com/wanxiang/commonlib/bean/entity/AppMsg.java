package com.wanxiang.commonlib.bean.entity;

import android.graphics.drawable.Drawable;

import java.util.Arrays;

public class AppMsg {
    String appName;
    String packName;
    long versionCode;
    String versionName;
    Drawable appIocn;
    String signature;
    String [] permissions;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(long versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Drawable getAppIocn() {
        return appIocn;
    }

    public void setAppIocn(Drawable appIocn) {
        this.appIocn = appIocn;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public void setPermissions(String[] permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "AppMsg{" +
                "appName='" + appName + '\'' +
                ", packName='" + packName + '\'' +
                ", versionCode=" + versionCode +
                ", versionName='" + versionName + '\'' +
                ", appIocn=" + appIocn +
                ", signature='" + signature + '\'' +
                ", permissions=" + Arrays.toString(permissions) +
                '}';
    }
}
