package com.wanxiang.commonlib.utils;

import android.annotation.SuppressLint;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.PowerManager;
import com.blankj.utilcode.util.SPUtils;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.base.MConstants;
import org.greenrobot.eventbus.EventBus;
import java.lang.reflect.Method;

/**
 * Created by lgj
 * 2020/6/11
 **/
public class ScreenUtils {


    private static final String TAG = "app:x7wake";


    /**
     * 是否亮屏
     *
     * @param context
     * @return
     */
    public static boolean isScreenOn(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //true为打开，false为关闭
        boolean isOpen = powerManager.isScreenOn();
        return isOpen;
    }


    public static void setScreenOn(Context context) {
        PowerManager mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock mWakeLock;
        if (!mPowerManager.isScreenOn()) {
            mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
            mWakeLock.acquire();
            mWakeLock.release();
            LogUtils.Logd(TAG, "屏幕亮屏");
        }

    }

    public static void setScreenOff(Context context) {
        PowerManager mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock mWakeLock;
         DevicePolicyManager policyManager = (DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);

        if (mPowerManager.isScreenOn()) {
            screenOff(context);
//            EventBus.getDefault().post(new ScreenSwitchEvent(1));
            LogUtils.Logd(TAG, "屏幕熄屏");
        }
    }


    private static void screenOff(Context context){
        DevicePolicyManager policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName adminReceiver = new ComponentName("com.deling.launcher", "com.deling.launcher.broadcast.ScreenOffAdminReceiver");

        checkAndTurnOnDeviceManager(policyManager,adminReceiver);
        boolean admin = policyManager.isAdminActive(adminReceiver);
        if (admin) {
            policyManager.lockNow();
        } else {

        }
    }

    private static void checkAndTurnOnDeviceManager(DevicePolicyManager policyManager,ComponentName adminReceiver) {
        if (!isOpen(policyManager,adminReceiver)) {
            ComponentName componentName = new ComponentName("com.deling.launcher", "com.deling.launcher.broadcast.ScreenOffAdminReceiver");
            try {
                DevicePolicyManager mDPM = (DevicePolicyManager) CommonLib.getContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
                Method setActiveAdmin = mDPM.getClass().getDeclaredMethod("setActiveAdmin", ComponentName.class, boolean.class);
                setActiveAdmin.setAccessible(true);
                setActiveAdmin.invoke(mDPM, componentName, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            LogUtils.Logd(TAG, " 权限已经打开...");
        }
    }

    private static boolean isOpen(DevicePolicyManager policyManager,ComponentName adminReceiver) {
        return policyManager.isAdminActive(adminReceiver);
    }


}
