package com.wanxiang.commonlib.utils;


import static android.content.Context.ACTIVITY_SERVICE;
import static com.wanxiang.commonlib.utils.CHIP_PLATFORM.ALLWINNER;
import static com.wanxiang.commonlib.utils.CHIP_PLATFORM.RK;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.usage.StorageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.SPUtils;
import com.wanxiang.commonlib.BuildConfig;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.base.MConstants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * ShellUtils
 * <ul>
 * <strong>Check root</strong>
 * <li>{@link ShellUtils#checkRootPermission()}</li>
 * </ul>
 * <ul>
 * <strong>Execte command</strong>
 * <li>{@link ShellUtils#execCommand(String, boolean)}</li>
 * <li>{@link ShellUtils#execCommand(String, boolean, boolean)}</li>
 * <li>{@link ShellUtils#execCommand(List, boolean)}</li>
 * <li>{@link ShellUtils#execCommand(List, boolean, boolean)}</li>
 * <li>{@link ShellUtils#execCommand(String[], boolean)}</li>
 * <li>{@link ShellUtils#execCommand(String[], boolean, boolean)}</li>
 * </ul>
 */
public class ShellUtils {

    private static final String TAG = "ShellUtils";

    public static final String COMMAND_SU = "su";
    // public static final String COMMAND_SU       = "/system/xbin/su";
    public static final String COMMAND_SH = "sh";
    public static final String COMMAND_EXIT = "exit\n";
    public static final String COMMAND_LINE_END = "\n";
    static final String ZGB_POWERUP_X6 = "echo w0b71 >   /sys/devices/virtual/ctf/demo_gpio/gpio";
    static final String ZGB_POWERUP_F7 = "echo w0b71 >   /sys/devices/virtual/ctf/demo_gpio/gpio";
    static final String ZGB_POWERUP_T8 = "echo 1 > /sys/devices/platform/zigbee_pwr/zigbee_pwr";
    static final String ZGB_POWERUP_B8 = "echo 1 > /sys/devices/platform/zigbee_pwr/zigbee_pwr";
    static final String ZGB_POWERUP_B10 = "echo 1 > /sys/devices/platform/zigbee_pwr/zigbee_pwr";
    static final String ZGB_POWERUP_X5 = "echo w3d31 >   /sys/devices/virtual/ctf/demo_gpio/gpio";
    static final String ZGB_POWERUP_TPP10 = "echo 1 >  /sys/devices/platform/zigbee_pwr/zigbee_pwr";


    public static final String POWER_POWERUP_B10 = "echo 1 > sys/devices/platform/bt_pwr/bt_pwr";

    public static final String CLOSE_POWER_POWERUP_B10 = "echo 0 > sys/devices/platform/bt_pwr/bt_pwr";

    public static final String ZGB_NODE_X6 = "/dev/ttyS5";
    public static final String ZGB_NODE_F7 = "/dev/ttyS5";
    public static final String ZGB_NODE_T8 = "/dev/ttyS0";
    public static final String ZGB_NODE_T8H = "/dev/ttyS8";
    public static final String ZGB_NODE_B10 = "/dev/ttyS0";
    public static final String ZGB_NODE_B8 = "/dev/ttyS0";
    public static final String ZGB_NODE_X8 = "/dev/ttyS0";
    public static final String ZGB_NODE_TPP10 = "/dev/ttyS5";
    public static final String CHIP_PLATFORM_ALLWINNER = "/dev/ttyS2";

//    public static final String POWER_POWER_OPENGPIO="echo 1 > /sys/devices/platform/mic_pwr/mic_pwr";//打开GPIO给K歌模块供电

    public static final String SOUND_SOURCERK809_PATH = "/sys/devices/platform/id1_ctl/id1_ctl";
    public static final String SOUND_SOURCEES8316_PATH = "/sys/devices/platform/id2_ctl/id2_ctl";

    public static final String SOUND_SOURCERK809_PATH_BLUETOOTH = "echo 3 > /sys/devices/platform/id1_ctl/id1_ctl";//分区1蓝牙输出
    public static final String SOUND_SOURCEES8316_PATH_BLUETOOTH = "echo 3 > /sys/devices/platform/id2_ctl/id2_ctl";//分区2蓝牙输出

    public static final String SOUND_SOURCERK809_PATH_LOCAL = "echo 0 > /sys/devices/platform/id1_ctl/id1_ctl";//分区1切到本地输出
    public static final String SOUND_SOURCEES8316_PATH_LOCAL = "echo 0 > /sys/devices/platform/id2_ctl/id2_ctl";//分区2切到本地输出

    public static final String SOUND_SOURCERK809_PATH_AUX = "echo 2 > /sys/devices/platform/id1_ctl/id1_ctl";//分区1切到外部输出
    public static final String SOUND_SOURCEES8316_PATH_AUX = "echo 2 > /sys/devices/platform/id2_ctl/id2_ctl";//分区2切到外部输出

    public static final String TPP10_SWITCH_ONE_OPEN = "echo k1 1 > /sys/class/tkctrl/gpios/zerro";//继电器1开
    public static final String TPP10_SWITCH_ONE_CLOSE = "echo k1 0 > /sys/class/tkctrl/gpios/zerro";//继电器1关

    public static final String ALLWINNER_SWITCH_ONE_OPEN = "echo k1 1 > /sys/devices/virtual/wsctrl/gpios/values";//继电器1开
    public static final String ALLWINNER_SWITCH_ONE_CLOSE = "echo k1 0 > /sys/devices/virtual/wsctrl/gpios/values";//继电器1关


    public static final String TPP10_SWITCH_TWO_OPEN = "echo k2 1 > /sys/class/tkctrl/gpios/zerro";//继电器2开
    public static final String TPP10_SWITCH_TWO_CLOSE = "echo k2 0 > /sys/class/tkctrl/gpios/zerro";//继电器2关
    public static final String ALLWINNER_SWITCH_TWO_OPEN = "echo k2 1 > /sys/devices/virtual/wsctrl/gpios/values";//继电器2开
    public static final String ALLWINNER_SWITCH_TWO_CLOSE = "echo k2 0 > /sys/devices/virtual/wsctrl/gpios/values";//继电器2关


    public static final String TPP10_SWITCH_QUERY = "/sys/class/tkctrl/gpios/zerro";//继电器是否连接
    public static final String ALLWINNER_SWITCH_QUERY = "/sys/devices/virtual/wsctrl/gpios/values";//继电器是否连接


    public static final String TPP10_SWITCH_ONE_STATE = "/sys/devices/platform/relay1_ctl/relay1_ctl";//继电器1状态

    public static final String TPP10_SWITCH_TWO_STATE = "/sys/devices/platform/relay2_ctl/relay2_ctl";//继电器2状态

    public static final String SOURCE_LOCAL = "1";                //本地
    public static final String SOURCE_SPTICALFIBER = "2";         // 光纤
    public static final String SOURCE_AUX = "3";                  // AUX
    public static final String SOURCE_BLE = "4";                  // BLE

    public static final String VOLUME_B10_AREA_ONE_PATH = "/sys/devices/platform/gpio_vol1/volume";
    public static final String VOLUME_B10_AREA_TWO_PATH = "/sys/devices/platform/gpio_vol2/volume";
    public static final String SMALL_PAD_VOICE_PATH = "/sys/devices/platform/mute_pwr/mute_pwr";

    public static final String VOLUME_X6_AREA_ONE_PATH = "/sys/class/rk618/volume";
    public static final String VOLUME_X6_AREA_TWO_PATH = "/sys/class/rk817/volume";


    public static final String AUX_PATH1 = "/sys/devices/platform/aux1_ctl/aux1_ctl";
    public static final String AUX_PATH2 = "/sys/devices/platform/aux2_ctl/aux2_ctl";

    public static final String AUX_PATH_ONE = "echo 1 > /sys/devices/platform/aux1_ctl/aux1_ctl";//打开分区一外部使能
    public static final String AUX_PATH_TWO = "echo 1 > /sys/devices/platform/aux2_ctl/aux2_ctl";//打开分区二外部使能

    public static final String AUX_PATH_ONE_CLOSE = "echo 0 > /sys/devices/platform/aux1_ctl/aux1_ctl";//关闭分区一外部使能
    public static final String AUX_PATH_TWO_CLOSE = "echo 0 > /sys/devices/platform/aux2_ctl/aux2_ctl";//关闭分区二外部使能

    public static final String X6_AUX_PATH = "/sys/class/rk618/switch";

    public static final String VOLUME_MIDDLE_ONE = "echo 7 > /sys/devices/platform/gpio_vol1/volume";//将分区一音量调到最大
    public static final String VOLUME_MIDDLE_TWO = "echo 7 > /sys/devices/platform/gpio_vol2/volume";//将分区二音量调到最大

    public static final String VOLUME_MIDDLE_ONE_CLOSE = "echo 1 > /sys/devices/platform/gpio_vol1/volume";//将分区一音量调到最小
    public static final String VOLUME_MIDDLE_TWO_CLOSE = "echo 1 > /sys/devices/platform/gpio_vol2/volume";//将分区二音量调到最小

    /*=============================================MaxPad8 三分区节点=========================================================================*/
    //外部BLE 开关
    public static final String MAXPAD8THREE_OUT_BLE_OPEN = "echo bt 1 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_OUT_BLE_CLOSE = "echo bt 0 > /sys/devices/virtual/wsctrl/gpios/values";

    // 分区音源
    public static final String MAXPAD8THREE_ZONE_ONE_BLE_SOURCE = "echo asw 0 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_TWO_BLE_SOURCE = "echo bsw 0 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_THREE_BLE_SOURCE = "echo csw 0 > /sys/devices/virtual/wsctrl/gpios/values";

    public static final String MAXPAD8THREE_ZONE_ONE_LOCAL_SOURCE = "echo asw 1 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_TWO_LOCAL_SOURCE = "echo bsw 1 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_THREE_LOCAL_SOURCE = "echo csw 1 > /sys/devices/virtual/wsctrl/gpios/values";

    public static final String MAXPAD8THREE_ZONE_ONE_AUX_SOURCE = "echo asw 2 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_TWO_AUX_SOURCE = "echo bsw 2 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_THREE_AUX_SOURCE = "echo csw 2 > /sys/devices/virtual/wsctrl/gpios/values";

/*
    // 底座分区功能使能控制
    public static final String MAXPAD8THREE_ZONE_ONE_ENABLE_OPEN = "echo p1 1 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_ONE_ENABLE_CLOSE = "echo p1 0 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_TWO_ENABLE_OPEN = "echo p2 1 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_TWO_ENABLE_CLOSE = "echo p2 0 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_THREE_ENABLE_OPEN = "echo p3 1 > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_THREE_ENABLE_CLOSE = "echo p3 0 > /sys/devices/virtual/wsctrl/gpios/values";
*/

    // 音量控制  控制值:0~31,0:静音,31:最大
    public static final String MAXPAD8THREE_ZONE_ROOT_VOLUME = " > /sys/devices/virtual/wsctrl/gpios/values";
    public static final String MAXPAD8THREE_ZONE_ONE_ALL_VOLUME_CMD = "echo aav  ";
    public static final String MAXPAD8THREE_ZONE_ONE_LEFT_VOLUME_CMD = "echo alv  ";
    public static final String MAXPAD8THREE_ZONE_ONE_RIGHT_VOLUME_CMD = "echo arv  ";

    public static final String MAXPAD8THREE_ZONE_TWO_ALL_VOLUME_CMD = "echo bav  ";
    public static final String MAXPAD8THREE_ZONE_TWO_LEFT_VOLUME_CMD = "echo blv  ";
    public static final String MAXPAD8THREE_ZONE_TWO_RIGHT_VOLUME_CMD = "echo brv  ";

    public static final String MAXPAD8THREE_ZONE_THREE_ALL_VOLUME_CMD = "echo cav  ";
    public static final String MAXPAD8THREE_ZONE_THREE_LEFT_VOLUME_CMD = "echo clv  ";
    public static final String MAXPAD8THREE_ZONE_THREE_RIGHT_VOLUME_CMD = "echo crv  ";
    // 底座分区使能控制

    public static final String MAXPAD8THREE_ZONE_ENABLE_PATH = " /sys/devices/virtual/wsctrl/gpios/values";

    public static final String MAXPAD8THREE_ZONE_ONE_ENABLE_OPEN_CMD = "echo p1 1 >";
    public static final String MAXPAD8THREE_ZONE_ONE_ENABLE_CLOSE_CMD = "echo p1 0 >";
    public static final String MAXPAD8THREE_ZONE_TWO_ENABLE_OPEN_CMD = "echo p2 1 >";
    public static final String MAXPAD8THREE_ZONE_TWO_ENABLE_CLOSE_CMD = "echo p2 0 >";
    public static final String MAXPAD8THREE_ZONE_THREE_ENABLE_OPEN_CMD = "echo p3 1 >";
    public static final String MAXPAD8THREE_ZONE_THREE_ENABLE_CLOSE_CMD = "echo p3 0 >";


    private static Handler handler = new Handler();

    private ShellUtils() {
        throw new AssertionError();
    }

    /**
     * check whether has root permission
     *
     * @return
     */
    public static boolean checkRootPermission() {
        return execCommand("echo root", true, false).result == 0;
    }

    /**
     * execute shell command, default return result msg
     *
     * @param command command
     * @param isRoot  whether need to run with root
     * @return
     * @see ShellUtils#execCommand(String[], boolean, boolean)
     */
    public static CommandResult execCommand(String command, boolean isRoot) {
        LogUtils.Logd(TAG," command:"+command);
        return execCommand(new String[]{command}, isRoot, true);
    }

    /**
     * execute shell commands, default return result msg
     *
     * @param commands command list
     * @param isRoot   whether need to run with root
     * @return
     * @see ShellUtils#execCommand(String[], boolean, boolean)
     */
    public static CommandResult execCommand(List<String> commands, boolean isRoot) {
        return execCommand(commands == null ? null : commands.toArray(new String[]{}), isRoot, true);
    }

    /**
     * execute shell commands, default return result msg
     *
     * @param commands command array
     * @param isRoot   whether need to run with root
     * @return
     * @see ShellUtils#execCommand(String[], boolean, boolean)
     */
    public static CommandResult execCommand(String[] commands, boolean isRoot) {
        return execCommand(commands, isRoot, true);
    }

    /**
     * execute shell command
     *
     * @param command         command
     * @param isRoot          whether need to run with root
     * @param isNeedResultMsg whether need result msg
     * @return
     * @see ShellUtils#execCommand(String[], boolean, boolean)
     */
    public static CommandResult execCommand(String command, boolean isRoot, boolean isNeedResultMsg) {
        return execCommand(new String[]{command}, isRoot, isNeedResultMsg);
    }

    /**
     * execute shell commands
     *
     * @param commands        command list
     * @param isRoot          whether need to run with root
     * @param isNeedResultMsg whether need result msg
     * @return
     * @see ShellUtils#execCommand(String[], boolean, boolean)
     */
    public static CommandResult execCommand(List<String> commands, boolean isRoot, boolean isNeedResultMsg) {
        return execCommand(commands == null ? null : commands.toArray(new String[]{}), isRoot, isNeedResultMsg);
    }

    public static void startVoice() {
        com.blankj.utilcode.util.LogUtils.dTag("doCmds", "startServiceTest");
        Process process;
        try {
            String command = "am start -n com.deling.voice/com.deling.lib_smart_home.view.StartServiceActivity";
            Runtime runtime = Runtime.getRuntime();
            process = runtime.exec(command);
            process.waitFor();
        } catch (Exception e) {
            com.blankj.utilcode.util.LogUtils.dTag("doCmds", e);
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startVoiceTwo();
                Log.d("ShellUtils", "第二次启动");
            }
        }, 3000);
    }

    public static void startVoiceTwo() {
        com.blankj.utilcode.util.LogUtils.dTag("doCmds", "startServiceTest");
        Process process;
        try {
            String command = "am start -n com.deling.voice/com.deling.lib_smart_home.view.StartServiceActivity";
            Runtime runtime = Runtime.getRuntime();
            process = runtime.exec(command);
            process.waitFor();
        } catch (Exception e) {
            com.blankj.utilcode.util.LogUtils.dTag("doCmds", e);
        }

    }

    /**
     * 将原有的手势效果进行修改
     */
    public static void changeGesture() {
        com.blankj.utilcode.util.LogUtils.dTag("doCmds", "startServiceTest");
        Process process;
        try {
            String command = "cmd overlay enable com.android.internal.systemui.navbar.gestural";
            Runtime runtime = Runtime.getRuntime();
            process = runtime.exec(command);
            process.waitFor();
        } catch (Exception e) {
            com.blankj.utilcode.util.LogUtils.dTag("doCmds", e);
        }
    }

    public static void turnOffMachine() {
        String voice = "reboot -p";
        execCommand(voice, false, true);
    }

    public static void reboot() {
        String voice = "reboot";
        execCommand(voice, false, true);
    }

    public static void rebootSwitchVoice() {
        CommonLib.getAsynHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String voice = "reboot";
                execCommand(voice, false, true);
            }
        }, 2000);
    }

    /**
     * execute shell commands
     *
     * @param commands        command array
     * @param isRoot          whether need to run with root
     * @param isNeedResultMsg whether need result msg
     * @return <ul>
     * <li>if isNeedResultMsg is false, {@link CommandResult#successMsg} is null and
     * {@link CommandResult#errorMsg} is null.</li>
     * <li>if {@link CommandResult#result} is -1, there maybe some excepiton.</li>
     * </ul>
     */
    public static CommandResult execCommand(String[] commands, boolean isRoot, boolean isNeedResultMsg) {
        int result = -1;
        if (commands == null || commands.length == 0) {
            return new CommandResult(result, null, null);
        }

        Process process = null;
        BufferedReader successResult = null;
        BufferedReader errorResult = null;
        StringBuilder successMsg = null;
        StringBuilder errorMsg = null;

        DataOutputStream os = null;
        try {
            if (BuildConfig.CHIP_PLATFORM.equals(RK.name())) {
                process = Runtime.getRuntime().exec(isRoot ? COMMAND_SU : COMMAND_SH);
            } else if (BuildConfig.CHIP_PLATFORM.equals(ALLWINNER.name())) {
                process = Runtime.getRuntime().exec(COMMAND_SH);
            }
            os = new DataOutputStream(process.getOutputStream());
            for (String command : commands) {
                if (command == null) {
                    continue;
                }
                // donnot use os.writeBytes(commmand), avoid chinese charset error
                os.write(command.getBytes());
                os.writeBytes(COMMAND_LINE_END);
                os.flush();
            }
            os.writeBytes(COMMAND_EXIT);
            os.flush();

            result = process.waitFor();
            if (isNeedResultMsg) {
                successMsg = new StringBuilder();
                errorMsg = new StringBuilder();
                successResult = new BufferedReader(new InputStreamReader(process.getInputStream()));
                errorResult = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String s;
                while ((s = successResult.readLine()) != null) {
                    successMsg.append(s);
                }
                while ((s = errorResult.readLine()) != null) {
                    errorMsg.append(s);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (successResult != null) {
                    successResult.close();
                }
                if (errorResult != null) {
                    errorResult.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (process != null) {
                process.destroy();
            }
        }
        return new CommandResult(result, successMsg == null ? null : successMsg.toString(), errorMsg == null ? null
                : errorMsg.toString());
    }

    /**
     * result of command
     * <ul>
     * <li>{@link CommandResult#result} means result of command, 0 means normal, else means error, same to excute in
     * linux shell</li>
     * <li>{@link CommandResult#successMsg} means success message of command result</li>
     * <li>{@link CommandResult#errorMsg} means error message of command result</li>
     * </ul>
     */
    public static class CommandResult {

        /**
         * result of command
         **/
        public int result;
        /**
         * success message of command result
         **/
        public String successMsg;
        /**
         * error message of command result
         **/
        public String errorMsg;

        public CommandResult(int result) {
            this.result = result;
        }

        public CommandResult(int result, String successMsg, String errorMsg) {
            this.result = result;
            this.successMsg = successMsg;
            this.errorMsg = errorMsg;
        }

        @Override
        public String toString() {
            return "CommandResult{" +
                    "result=" + result +
                    ", successMsg='" + successMsg + '\'' +
                    ", errorMsg='" + errorMsg + '\'' +
                    '}';
        }
    }

    public static String getSN() {
        if (TextUtils.isEmpty(CommonLib.deviceId)) {
            String SN = CommonLib.deviceId = PropertiesUtils.get("ro.serialno");
//            if (!AppChannel.Config.HASKUGOU) {
//                SN = CommonLib.deviceId = "GRBV16998";
//            }
            return SN;
        }
        LogUtils.Logd(TAG, "CommonLib.deviceId:" + CommonLib.deviceId);
        return CommonLib.deviceId;

    }


    /**
     * sdcard 可用大小
     *
     * @param sdcardPath sdcard 根路径
     * @return 单位为：byte
     */
    public static long getAvailableExternalMemorySize(String sdcardPath) {
        try {
            if (isExternalStorageAvailable()) {
                if (TextUtils.isEmpty(sdcardPath)) {
                    return 0;
                }

                // 尝试多加判断，如果无效的参数 StatFs 会报错
                File file = new File(sdcardPath);
                boolean isSymLink = false;
                try {
                    isSymLink = isSymlink(file);
                } catch (IOException e) {
                    e.printStackTrace();
                    return 0;
                }

                if (!file.exists() || !file.isDirectory() || isSymLink) {
                    return 0;
                }

                StatFs stat = new StatFs(sdcardPath);
                long blockSize = stat.getBlockSize();
                long availableBlocks = stat.getAvailableBlocks();
                return availableBlocks * blockSize;
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static boolean isSymlink(File file) throws IOException {
        if (file == null) {
            throw new NullPointerException("File must not be null");
        }

        File canon;
        if (file.getParent() == null) {
            canon = file;
        } else {
            File canonDir = file.getParentFile().getCanonicalFile();
            canon = new File(canonDir, file.getName());
        }
        return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
    }

    /**
     * 外部存储是否可用 (存在且具有读写权限)
     *
     * @return
     */
    public static boolean isExternalStorageAvailable() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public long getTotalSize(String fsUuid) {
        try {
            UUID id;
            if (fsUuid == null) {
                id = StorageManager.UUID_DEFAULT;
            } else {
                id = UUID.fromString(fsUuid);
            }
            StorageStatsManager stats = CommonLib.getContext().getSystemService(StorageStatsManager.class);
            return stats.getTotalBytes(id);
        } catch (NoSuchFieldError | NoClassDefFoundError | NullPointerException | IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void installApk(String apkFilePath) {
        LogUtils.Logd(TAG, "installApk  apkFilePath:" + apkFilePath);
        File file = new File(apkFilePath);
        if (!file.exists() || !file.isFile()) {
            LogUtils.Logd(TAG, "安装apk 文件错误");
            return;
        }
        PackageManager packageManager = CommonLib.getContext().getPackageManager();
        boolean success = false;
        try {
            Class<?> clazz = CommonLib.getContext().getClassLoader().loadClass("android.app.PackageInstallObserver");
            Method method = packageManager.getClass().getDeclaredMethod("installPackage",
                    Uri.class, clazz, int.class, String.class);
            method.setAccessible(true);
            //|0x00000040|0x00001000
            method.invoke(packageManager, Uri.fromFile(file), clazz.newInstance(), 0x00000002, "com.deling.launcher");
            success = true;
        } catch (Exception e) {
            LogUtils.Logd(TAG, "method.invoke  安装失败！");
            e.printStackTrace();
        } finally {
            if (!success) {
                LogUtils.Logd(TAG, "HandlerConstanst.mApkInstallFailed");
            } else {
                LogUtils.Logd(TAG, "installApk ApkUpdateEnum.INSTALLING");
            }
        }
    }


    public static String getForegroundApp(Context context) {
        ActivityManager am =
                (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> lr = am.getRunningAppProcesses();
        if (lr == null) {
            return null;
        }

        for (ActivityManager.RunningAppProcessInfo ra : lr) {
            if (ra.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE
                    || ra.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return ra.processName;
            }
        }
        return null;
    }


    public static void openVolumeControl() {
        List<String> list = new ArrayList<>();
        String l = AUX_PATH_ONE;
        String r = AUX_PATH_TWO;
        list.add(l);
        list.add(r);
        execCommand(list, true, true);
    }

    public static void openVolumeControlClose() {
//        if(OPEN_KG_STATE ==CacheMemoryUtils.getInstance().get(IS_KG,CLOSE_KG_STATE) &&AppChannel.getCurrentConfig().isHasKG()){
//            return;
//        }
        List<String> list = new ArrayList<>();
        String l = AUX_PATH_ONE_CLOSE;
        String r = AUX_PATH_TWO_CLOSE;
        list.add(l);
        list.add(r);
        execCommand(list, true, true);
    }

    public static void openVolumeControlOne() {
        execCommand(AUX_PATH_ONE, true, true);
    }

    public static void closeVolumeControlTwo() {
//        if(OPEN_KG_STATE == CacheMemoryUtils.getInstance().get(IS_KG,CLOSE_KG_STATE) &&AppChannel.getCurrentConfig().isHasKG()){
//            return;
//        }
        execCommand(AUX_PATH_TWO_CLOSE, true, true);
    }

    public static void closeVolumeControlOne() {
//        if(OPEN_KG_STATE ==CacheMemoryUtils.getInstance().get(IS_KG,CLOSE_KG_STATE) &&AppChannel.getCurrentConfig().isHasKG()){
//            return;
//        }
        execCommand(AUX_PATH_ONE_CLOSE, true, true);
    }

    public static void openVolumeControTwo() {
        execCommand(AUX_PATH_TWO, true, true);
    }

    public static void volumeMax() {
        execCommand(AUX_PATH_TWO, true, true);
    }

    public static void openMusicVoice() {
        List<String> list = new ArrayList<>();
        String l = "echo 1 > /sys/class/gpio/gpio113/value";
        String r = "echo 1 > /sys/class/gpio/gpio24/value";
        list.add(l);
        list.add(r);
        execCommand(list, true, true);
    }

    //打开pad小喇叭
    public static void openPadVoice() {
        LogUtils.Logd(TAG, "openPadVoice");
        int voiceState = (int) SharePreferenceUtils.get(CommonLib.getContext(), MConstants.PAD_VOICE_STATE, 0);
        String padState = (String) SharePreferenceUtils.get(CommonLib.getContext(), MConstants.PAD_INSERT_OUT_STATE, "1");
        if (padState.equals("1") || voiceState == 1) {
            String voice = "echo 1 >  " + SMALL_PAD_VOICE_PATH;
            execCommand(voice, true, false);
        }
    }

    //关闭pad小喇叭
    public static void closePadVoice() {
        LogUtils.Logd(TAG, "closePadVoice");
        String voice = "echo 0 >  " + SMALL_PAD_VOICE_PATH;
        execCommand(voice, true, false);
    }

    public static void closeMusicVoice() {
        List<String> list = new ArrayList<>();
        String l = "echo 0 > /sys/class/gpio/gpio113/value";
        String r = "echo 0 > /sys/class/gpio/gpio24/value";
        list.add(l);
        list.add(r);
        execCommand(list, false, true);
    }

    public static void openAiVoice() {
        String voice = "echo 1 > /sys/class/gpio/gpio30/value";
        execCommand(voice, false, true);
    }

    public static void closeAiVoice() {
        String voice = "echo 0 > /sys/class/gpio/gpio30/value";
        execCommand(voice, false, true);
    }


    public static void goBack() {
        String cmd = " input keyevent 4 ";
        execCommand(cmd, true, true);
    }

    public static void bluetoothElectric() {
        execCommand(POWER_POWERUP_B10, true, true);
    }

    public static void closeBluetoothElectric() {
        execCommand(CLOSE_POWER_POWERUP_B10, true, true);
    }


    public static void openBlueToothOne() {
        execCommand(SOUND_SOURCERK809_PATH_BLUETOOTH, true, true);
    }

    public static void openBlueToothTwo() {
        execCommand(SOUND_SOURCEES8316_PATH_BLUETOOTH, true, true);
    }

    public static void openLocalOne() {
        execCommand(SOUND_SOURCERK809_PATH_LOCAL, true, true);
    }

    public static void openJdqOne() {
        if (BuildConfig.CHIP_PLATFORM.equals(ALLWINNER.name())) {
            execCommand(ALLWINNER_SWITCH_ONE_OPEN, true, true);
        } else if (BuildConfig.CHIP_PLATFORM.equals(RK.name())) {
            execCommand(TPP10_SWITCH_ONE_OPEN, true, true);
        }
    }

    public static void openJdqTwo() {
        if (BuildConfig.CHIP_PLATFORM.equals(ALLWINNER.name())) {
            execCommand(ALLWINNER_SWITCH_TWO_OPEN, true, true);
        } else if (BuildConfig.CHIP_PLATFORM.equals(RK.name())) {
            execCommand(TPP10_SWITCH_TWO_OPEN, true, true);
        }
    }

    public static void closeJdqOne() {
        if (BuildConfig.CHIP_PLATFORM.equals(ALLWINNER.name())) {
            execCommand(ALLWINNER_SWITCH_ONE_CLOSE, true, true);
        } else if (BuildConfig.CHIP_PLATFORM.equals(RK.name())) {
            execCommand(TPP10_SWITCH_ONE_CLOSE, true, true);
        }
    }

    public static void closeJdqTwo() {

        if (BuildConfig.CHIP_PLATFORM.equals(ALLWINNER.name())) {
            execCommand(ALLWINNER_SWITCH_TWO_CLOSE, true, true);
        } else if (BuildConfig.CHIP_PLATFORM.equals(RK.name())) {
            execCommand(TPP10_SWITCH_TWO_CLOSE, true, true);
        }

    }

    public static void openLocalTwo() {
        execCommand(SOUND_SOURCEES8316_PATH_LOCAL, true, true);
    }

    public static void openAUXOne() {
        execCommand(SOUND_SOURCERK809_PATH_AUX, true, true);
    }

    public static void openAUXTwo() {
        execCommand(SOUND_SOURCEES8316_PATH_AUX, true, true);
    }

    public static void openVolumeSizeOne() {
        execCommand(VOLUME_MIDDLE_ONE, true, true);
    }

    public static void adjustVolume(int volume) {
        execCommand("echo " + volume + " > /sys/devices/platform/gpio_vol1/volume", true, true);
    }

    public static void adjustVolumeTwo(int volume) {
        execCommand("echo " + volume + " > /sys/devices/platform/gpio_vol2/volume", true, true);
    }

    public static void openVolumeSizeTwo() {
        execCommand(VOLUME_MIDDLE_TWO, true, true);
    }

    public static void openVolumeSizeTwoClose() {
        execCommand(VOLUME_MIDDLE_TWO_CLOSE, true, true);
    }

    public static void openVolumeSizeOneClose() {
        execCommand(VOLUME_MIDDLE_ONE_CLOSE, true, true);
    }

//    public static void openGpio(){
//        execCommand(POWER_POWER_OPENGPIO, true, true);
//    }

    /**
     * 获取手机序列号
     *
     * @return 手机序列号
     */
    @SuppressLint({"NewApi", "MissingPermission"})
    public static String getSerialNumber() {
        String serial = "";
        try {
            serial = com.wanxiang.commonlib.utils.ShellUtils.getSN() == null ? DeviceUtils.getMacAddress() : com.wanxiang.commonlib.utils.ShellUtils.getSN();
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.e("e", "读取设备序列号异常：" + e.toString());
        }
        return serial;
    }

    public static void deleteOtaFile() {
        File filePart = new File("/data", "/update.zip.0.part");
        if (filePart.exists()) filePart.delete();
        File file1Part = new File("/data", "/update.zip.1.part");
        if (file1Part.exists()) file1Part.delete();

        File file2Part = new File("/data", "/update.zip.2.part");
        if (file2Part.exists()) file2Part.delete();

        File fileOta = new File("/data", "/update.zip");
        if (fileOta.exists()) fileOta.delete();

        File filePart1 = new File("/sdcard", "/update.zip.0.part");
        if (filePart1.exists()) filePart1.delete();

        File file1Part2 = new File("/sdcard", "/update.zip.1.part");
        if (file1Part2.exists()) file1Part2.delete();

        File file2Part2 = new File("/sdcard", "/update.zip.2.part");
        if (file2Part2.exists()) file2Part2.delete();

        File fileOta3 = new File("/sdcard", "/update.zip");
        if (fileOta3.exists()) fileOta3.delete();
    }

    public static void OtaSuccess() {
        com.blankj.utilcode.util.LogUtils.dTag("doCmds", "startServiceTest");
        java.lang.Process process;
        try {
            //String command = "am start -W -n com.softwinner.update/com.softwinner.shared.CopyPackageActivity  --es path /sdcard/update.zip";
            String command = "am start -W -n com.softwinner.update/com.softwinner.shared.CopyPackageActivity  --es path  /data/update.zip";
            Runtime runtime = Runtime.getRuntime();
            process = runtime.exec(command);
            process.waitFor();
        } catch (Exception e) {
            com.blankj.utilcode.util.LogUtils.dTag("doCmds", e);
        }
    }

    public static void deviceReboot() {
        Intent i = new Intent("android.intent.action.REBOOT");
        // 立即重启：1
        i.putExtra("nowait", 1);
        // 重启次数：1
        i.putExtra("interval", 1);
        // 不出现弹窗：0
        i.putExtra("window", 0);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        CommonLib.getContext().startActivity(i);

    }

    public static void isReboot() {
        boolean aBoolean = SPUtils.getInstance().getBoolean(MConstants.LAUNCHER_INSTALL_IS_SUCCESS);
        Log.w(TAG, "isReboot: " + aBoolean);
        if (aBoolean) {
            SPUtils.getInstance().put(MConstants.LAUNCHER_INSTALL_IS_SUCCESS, false);
            SPUtils.getInstance().put(com.wanxiang.commonlib.base.MConstants.IS_REGISTER_DEVICE, false);
            LogUtils.Logd(TAG, "launcher刚安装过,准备重启");
            SPUtils.getInstance().put(MConstants.UP_SUCCESS_HINT_WINDOW, true);
            CommonLib.getAsynHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    com.wanxiang.commonlib.utils.ShellUtils.reboot();
                }
            }, 1000);

        }
    }

    public static void cleanDataByPkg(String pkg) {
        //  String voice = "echo l > /sys/codec-spk-ctl/spk-ctl"+pkg;
        String cmd = " pm clear " + pkg;
        LogUtils.Logd(TAG, "cleanDataByPkg: " + cmd);
        execCommand(cmd, true, true);
    }


    /**
     * 中控网关zgb 断电
     */
    public static void closeElectric() {
      /*  String voice = "echo 0 > /sys/devices/platform/zigbee_pwr/zigbee_pwr";         //T8
        execCommand(voice, true, true);
        if(AppChannel.currentConfig.getChannel().equals(AppChannel.T8.name())){
            execCommand(ZGB_POWERUP_T8, true, true);
        }else if(AppChannel.currentConfig.getChannel().equals(AppChannel.B10H.name())){
            execCommand(ZGB_POWERUP_B10, true, true);
        }*/
        LogUtils.Logd(TAG, "中控网关断电");
    }


    public static String getProcessName() {
        int pID = android.os.Process.myPid();
        String processName = null;
        ActivityManager am = (ActivityManager) CommonLib.getContext().getSystemService(ACTIVITY_SERVICE);
        List l = am.getRunningAppProcesses();
        Iterator i = l.iterator();
        PackageManager pm = CommonLib.getContext().getPackageManager();
        while (i.hasNext()) {
            ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i
                    .next());
            try {
                if (info.pid == pID) {
                    CharSequence c = pm.getApplicationLabel(pm
                            .getApplicationInfo(info.processName,
                                    PackageManager.GET_META_DATA));
                    processName = info.processName;
                    return processName;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return processName;

    }


    public static void startVoiceX7() {
        CommonLib.getAsynHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Process process;
                try {
                    String command = "am startservice -n com.deling.voice/com.deling.lib_dui.DuiService -a com.dui.DerServer";
                    Runtime runtime = Runtime.getRuntime();
                    process = runtime.exec(command);
                    process.waitFor();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 20 * 1000);
    }

    public static void stopAnimation() {
        //  PropertiesUtils.set("service.bootanim.exit", "1");
        CommonLib.getAsynHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LogUtils.Logd(TAG, "去结束开机动画");
                PropertiesUtils.set("service.bootanim.exit", "1");
            }
        }, 10 * 1000);
    }

    public static void openHistoricalTask() {
        Process process;
        try {
            String command = "input keyevent 187";
            Runtime runtime = Runtime.getRuntime();
            process = runtime.exec(command);
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //新加指令,打开k歌
    public static void openKge() {
        String open = "echo 1 > /sys/class/rk618/spk";
        execCommand(open, true, true);
    }

    public static void openGpio() {
        String open = "echo 1 > /sys/devices/platform/mic_pwr/mic_pwr";
        execCommand(open, true, true);
    }

    //新加指令,关闭k歌
    public static void closeKge() {
        String close = "echo 0 > /sys/class/rk618/spk";
        execCommand(close, true, true);
    }

    public static void closeGpio() {
//        String close  = "echo w2b400 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        String close = "echo 0 > /sys/devices/platform/mic_pwr/mic_pwr";
        execCommand(close, true, true);
    }

    //一分区 话筒，音乐 都有声音
    public static void openAll_one() {
        String music_one = "echo w2a51 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(music_one, true, true);

        String mircophone_one = "echo w2a41 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(mircophone_one, true, true);
    }

    //一分区 音乐有声音
    public static void openMusic_one() {
        String voice_one = "echo w2a50 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(voice_one, true, true);

        String mircophone_one = "echo w2a40 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(mircophone_one, true, true);
    }

    //一分区 话筒有声音
    public static void openMicrophone_one() {
        String voice_one = "echo w2a50 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(voice_one, true, true);
    }

    //二分区 话筒，音乐 都有声音
    public static void openAll_two() {
        String music_one = "echo w2a51 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(music_one, true, true);

        String mircophone_one = "echo w2a31 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(mircophone_one, true, true);
    }

    //二分区 音乐有声音
    public static void openMusic_two() {
        String voice_one = "echo w2a50 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(voice_one, true, true);

        String mircophone_one = "echo w2a30 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(mircophone_one, true, true);
    }

    //二分区 话筒有声音
    public static void openMicrophone_two() {
        String voice_one = "echo w2a50 > /sys/devices/virtual/ctf/demo_gpio/gpio";
        execCommand(voice_one, true, true);
    }

    //打开后切换到话筒MIC输入
    public static void openMicVolueInput() {
        String mic_volue = "echo 1 > /sys/devices/platform/mic_in_ctl/mic_in_ctl";
        execCommand(mic_volue, true, true);
    }

    //话筒输出和RK809本地播放切换
    public static void openMicVolueOutput() {
        //话筒输出和RK809本地播放切换
        String mic_volume = "echo 1 > /sys/devices/platform/audio1_ctl/audio1_ctl";
        execCommand(mic_volume, true, true);
        //话筒输出和ES8316本地播放切换
        String mic_volume1 = "echo 1 > /sys/devices/platform/audio2_ctl/audio2_ctl";
        execCommand(mic_volume1, true, true);
    }

    //关闭话筒MIC输入
    public static void closeMicVolueInput() {
        String mic_volue = "echo 0 > /sys/devices/platform/mic_in_ctl/mic_in_ctl";
        execCommand(mic_volue, true, true);
    }

    //话关闭筒输出和RK809本地播放切换
    public static void closeMicVolueOutput() {
        //话筒输出和RK809本地播放切换
        String mic_volume = "echo 0 > /sys/devices/platform/audio1_ctl/audio1_ctl";
        execCommand(mic_volume, true, true);
        //话筒输出和ES8316本地播放切换
        String mic_volume1 = "echo 0 > /sys/devices/platform/audio2_ctl/audio2_ctl";
        execCommand(mic_volume1, true, true);
    }


    /*
    zone:分区号
    flag:使能开关
     */
    public static void maxPadThreZoneEnable(int zone, boolean flag) {
        switch (zone) {
            case 0:
                if (flag)
                    execCommand(MAXPAD8THREE_ZONE_ONE_ENABLE_OPEN_CMD + MAXPAD8THREE_ZONE_ENABLE_PATH, true);
                else
                    execCommand(MAXPAD8THREE_ZONE_ONE_ENABLE_CLOSE_CMD + MAXPAD8THREE_ZONE_ENABLE_PATH, true);
                break;
            case 1:
                if (flag)
                    execCommand(MAXPAD8THREE_ZONE_TWO_ENABLE_OPEN_CMD + MAXPAD8THREE_ZONE_ENABLE_PATH, true);
                else
                    execCommand(MAXPAD8THREE_ZONE_TWO_ENABLE_CLOSE_CMD + MAXPAD8THREE_ZONE_ENABLE_PATH, true);
                break;
            case 2:
                if (flag)
                    execCommand(MAXPAD8THREE_ZONE_THREE_ENABLE_OPEN_CMD + MAXPAD8THREE_ZONE_ENABLE_PATH, true);
                else
                    execCommand(MAXPAD8THREE_ZONE_THREE_ENABLE_CLOSE_CMD + MAXPAD8THREE_ZONE_ENABLE_PATH, true);
                break;

        }
    }

    /*
    zone:分区号
    type: 音量大小类型
    volumeSize:声音大小
     */
    public static void maxPadThreVolumeSet(int zone, MaxPadThreeVolume type, int volumeSize) {

        if (type.name().equals(MaxPadThreeVolume.TOTAL.name())) {
            switch (zone) {
                case 0:
                    execCommand(MAXPAD8THREE_ZONE_ONE_ALL_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
                case 1:
                    execCommand(MAXPAD8THREE_ZONE_TWO_ALL_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
                case 2:
                    execCommand(MAXPAD8THREE_ZONE_THREE_ALL_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
            }

        } else if (type.name().equals(MaxPadThreeVolume.LEFT.name())) {
            switch (zone) {
                case 0:
                    execCommand(MAXPAD8THREE_ZONE_ONE_LEFT_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
                case 1:
                    execCommand(MAXPAD8THREE_ZONE_TWO_LEFT_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
                case 2:
                    execCommand(MAXPAD8THREE_ZONE_THREE_LEFT_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
            }
        } else if (type.name().equals(MaxPadThreeVolume.RIGHT.name())) {
            switch (zone) {
                case 0:
                    execCommand(MAXPAD8THREE_ZONE_ONE_RIGHT_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
                case 1:
                    execCommand(MAXPAD8THREE_ZONE_TWO_RIGHT_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;
                case 2:
                    execCommand(MAXPAD8THREE_ZONE_THREE_RIGHT_VOLUME_CMD+volumeSize+MAXPAD8THREE_ZONE_ROOT_VOLUME,true);
                    break;

            }
        }
    }


    public static void setMaxpad8threeOutBleOpen(boolean flag){
        if(flag){
            execCommand(MAXPAD8THREE_OUT_BLE_OPEN,true);
        }else{
            execCommand(MAXPAD8THREE_OUT_BLE_CLOSE,false);
        }
    }



    public static void maxPadThreZoneVoiceSource(int zone,   MaxPadThreeVoiceSource type) {
        if (type.name().equals(MaxPadThreeVoiceSource.BLE.name())) {
            switch (zone) {
                case 0:
                    execCommand(MAXPAD8THREE_ZONE_ONE_BLE_SOURCE,true);
                    break;
                case 1:
                    execCommand(MAXPAD8THREE_ZONE_TWO_BLE_SOURCE,true);
                    break;
                case 2:
                    execCommand(MAXPAD8THREE_ZONE_THREE_BLE_SOURCE,true);
                    break;
            }

        } else if (type.name().equals(MaxPadThreeVoiceSource.LOCAL.name())) {
            switch (zone) {
                case 0:
                    execCommand(MAXPAD8THREE_ZONE_ONE_LOCAL_SOURCE,true);
                    break;
                case 1:
                    execCommand(MAXPAD8THREE_ZONE_TWO_LOCAL_SOURCE,true);
                    break;
                case 2:
                    execCommand(MAXPAD8THREE_ZONE_THREE_LOCAL_SOURCE,true);
                    break;
            }
        } else if (type.name().equals(MaxPadThreeVoiceSource.AUX.name())) {
            switch (zone) {
                case 0:
                    execCommand(MAXPAD8THREE_ZONE_ONE_AUX_SOURCE,true);
                    break;
                case 1:
                    execCommand(MAXPAD8THREE_ZONE_TWO_AUX_SOURCE,true);
                    break;
                case 2:
                    execCommand(MAXPAD8THREE_ZONE_THREE_AUX_SOURCE,true);
                    break;

            }
        }
    }



}
