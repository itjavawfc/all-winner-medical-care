package com.wanxiang.commonlib.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.FileProvider;

import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.bean.entity.AppMsg;

import java.io.File;
import java.io.IOException;

public class ApkUtils {
    private static final String TAG = "ApkUtils";

    public static boolean installApp(String apkFilePath) {
        LogUtils.Logd(TAG, "installApp: 准备安装apk");
//        String install = "pm install -r /data/x7_launcher_20200806_v2.00.apk";
        String install = "pm install -r " + apkFilePath;
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand(install, true, true);
        LogUtils.Logd(TAG, "installApp: 命令执行结束:- " + commandResult);
        String successMsg = commandResult.successMsg;
        return successMsg != null && !(successMsg.isEmpty());
    }
    public static void installApk(String filleApkPath,String launcherPath) throws IOException {
        String exec="pm install -r  "+ launcherPath;//+" ; "+"pm install "+ launcherPath;//+" ;"+" reboot";
        Runtime.getRuntime().exec(exec);
        LogUtils.Logd(TAG,"  ....");
    }

    public static void systemInstallApp(String filePath) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        File file = new File(filePath);
        if (Build.VERSION.SDK_INT >= 24) {//大于7.0使用此方法
            Uri apkUri = FileProvider.getUriForFile(CommonLib.getContext(), "com.deling.launcher.fileProvider", file);///-----ide文件提供者名
            //添加这一句表示对目标应用临时授权该Uri所代表的文件
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {//小于7.0就简单了
            // 由于没有在Activity环境下启动Activity,设置下面的标签
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        }
        CommonLib.getContext().startActivity(intent);
    }


    /**
     * 获取程序的VersionName(用户看到的版本号)
     *
     * @param context
     * @param packName
     * @return
     */
    public static String getAppVersionName(Context context, String packName) {
        //包管理操作管理类
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packinfo = pm.getPackageInfo(packName, 0);
            return packinfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取程序的VersionCode(软件内部版本的标识)
     *
     * @param context
     * @param packName
     * @return
     */
    public static long getAppVersionCode(Context context, String packName) {
        //包管理操作管理类
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packinfo = pm.getPackageInfo(packName, 0);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {//当前api等级小于28
                return packinfo.versionCode;
            } else {
                return packinfo.getLongVersionCode();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 获取程序的名字
     *
     * @param context
     * @param packname
     * @return
     */
    public static String getAppName(Context context, String packname) {
        //包管理操作管理类
        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo info = pm.getApplicationInfo(packname, 0);
            return info.loadLabel(pm).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packname;
    }

    /**
     * 获取程序 图标
     *
     * @param context
     * @param packName 应用包名
     * @return
     */
    public static Drawable getAppIcon(Context context, String packName) {
        try {
            //包管理操作管理类
            PackageManager pm = context.getPackageManager();
            //获取到应用信息
            ApplicationInfo info = pm.getApplicationInfo(packName, 0);
            return info.loadIcon(pm);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取程序的权限
     **/
    public static String[] getAllPermissions(Context context, String packname) {
        try {
            //包管理操作管理类
            PackageManager pm = context.getPackageManager();
            PackageInfo packinfo = pm.getPackageInfo(packname, PackageManager.GET_PERMISSIONS);
            //获取到所有的权限
            return packinfo.requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }
        return null;
    }

    /**
     * 获取程序的签名
     *
     * @param context
     * @param packname
     * @return
     */
    public static String getAppSignature(Context context, String packname) {
        try {
            //包管理操作管理类
            PackageManager pm = context.getPackageManager();
            PackageInfo packinfo = pm.getPackageInfo(packname, PackageManager.GET_SIGNATURES);
            //获取当前应用签名
            return packinfo.signatures[0].toCharsString();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }
        return packname;
    }

    /**
     * 获取当前展示 的Activity名称
     *
     * @return
     */
    private static String getCurrentActivityName(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String runningActivity = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
        return runningActivity;
    }

    public static AppMsg getApkMsg(Context context, String packName) {
        AppMsg appMsg = new AppMsg();
        appMsg.setAppName(getAppName(context, packName));
        appMsg.setPackName(packName);
        appMsg.setVersionName(getAppVersionName(context, packName));
        appMsg.setVersionCode(getAppVersionCode(context, packName));
        appMsg.setAppIocn(getAppIcon(context, packName));
        appMsg.setPermissions(getAllPermissions(context, packName));
        appMsg.setSignature(getAppSignature(context, packName));
        return appMsg;
    }
}
