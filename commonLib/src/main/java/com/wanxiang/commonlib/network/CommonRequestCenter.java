package com.wanxiang.commonlib.network;


public class CommonRequestCenter {

    public static final int HTTP_OK = 200;
    private static final String ROOT_URL = "https://intel.delingsmart.cn";
//    private static final String ROOT_URL = "http://192.168.101.199:9100";
    public static String MQTT_URL = "tcp://intel.delingsmart.cn:1883";   //(正式环境)
//    public static String MQTT_URL = "tcp://192.168.101.199:1883";

//    private static final String AI_URL = "192.168.101.199:9100";AI测试

    public final static String Update_Group_Relation_Name = ROOT_URL + "/deling/common/relation/updateGroupRelationName";

    /**
     * 手机端获取设备端咪咕音乐播放记录
     */
    public final static String GET_HISTORY_MUSIC = ROOT_URL + "/deling/common/history/getHistoryMusic";

    //设备端上传正在播放的咪咕音频信息
    public final static String FIND_RELATIONS = ROOT_URL + "/deling/common/relation/findRelationByDevicieId"; //获取masterId 的绑定关系
    public final static String FIND_GROUP_RELATIONS = ROOT_URL + "/deling/common/relation/getGroupRelations"; //获取masterId 的绑定关系
    public final static String BUILD_GROUP_RELATIONS = ROOT_URL + "/deling/common/relation/buildGroupRelation"; //设备端组网操作
//    public final static String FIND_MUSIC_ALARMS= ROOT_URL + "/deling/device/alarm/getAlarmMusic";        //获取设备的定时提醒信息
//    public final static String DEL_MUSIC_ALARMS= ROOT_URL + "/deling/device/alarm/delAlarmMusic";        //设备端删除定时音乐信息
//    public final static String UPLOAD_MUSIC_ALARMS= ROOT_URL + "/deling/device/upload/uploadAlarmMsg";        //设备端上传定时音乐接口

    public final static String FIND_MUSIC_ALARMS = ROOT_URL + "/deling/device/alarm/getLoopAlarmMusic";        //获取设备的定时提醒信息
    public final static String DEL_MUSIC_ALARMS = ROOT_URL + "/deling/device/alarm/delLoopAlarmMusic";        //设备端删除定时音乐信息
    public final static String UPLOAD_MUSIC_ALARMS = ROOT_URL + "/deling/device/upload/uploadLoopAlarmMsg";        //设备端上传定时音乐接口

    public final static String UPLOAD_DEVICE_ZONE_NAME = ROOT_URL + "/deling/device/upload/upDeviceZoneName";        //设备端上传分区名称
    public final static String CTL_DEVICE_UPDATE_GROUPSYC = ROOT_URL + "/deling/common/relation/updateGroupSyc";
    public final static String CTL_DEVICE_UPDATE_GROUPNAME = ROOT_URL + "/deling/common/relation/updateGroupRelationName";
    public final static String DE_LING_SMART = ROOT_URL;
    //天气
    public final static String GET_WEATHER_URL = "https://apis.duiopen.com/apis/weather/forecast/specific";

    public final static String GET_DEVICE_LOCATION = ROOT_URL + "/deling/device/info/getLocation";
//    public final static String GET_WEATHER_URL="https://apis.duiopen.com/apis/weather/forecast/specific";

    /**
     * 设备端上传正在播放的咪咕音频信息
     */
    public final static String UPLOAD_DEVICE_MUSIC = ROOT_URL + "/deling/device/upload/uploadHistoryMusic";     //每次播放都需要上报当前正在播放的音乐信息
    public final static String UPLOAD_DEVICE_MUSICS = ROOT_URL + "/deling/device/upload/uploadDeviceMusics";     //上传音乐列表信息
    public final static String UPDATE_DEVICE_MUSICSS_STATES = ROOT_URL + "/deling/device/upload/updateMusicState";     //上报音乐列表状态，更新了新的音乐列表信息
    public final static String ADD_CONTROLL_DEVICE = ROOT_URL + "/deling/device/user/addControlDevice";          //注册设备
    public final static String ADD_CONTROLL_DEVICE_v2 = ROOT_URL + "/deling/device/user/v2/addControlDevice";          //V2注册设备
    public final static String OVER_REPORT = ROOT_URL + "/deling/device/upload/uploadMsg"; //整体上报数据,开机后整体上报数据
    public final static String PUSH_UPLOAD_MUSIC_PIC = ROOT_URL + "/deling/device/upload/pushUploadMusicPics";//上传本地图片   方便手机端显示设备端歌曲友好话
    public final static String UPLOAD_LINKAGEZONE = ROOT_URL + "/deling/device/upload/updateLinkage";     //设备端分区联动上传
    public final static String UPLOAD_MSG = ROOT_URL + "/deling/device/upload/updateUploadMsg";     //设备端上传设备状态
    public final static String UPLOAD_MUSIC_PROCESS_MSG = ROOT_URL + "/deling/device/upload/uploadMusicPlayProcess";     //设备端上传设备状态
    public final static String DELETE_HISTORY_MUSIC = ROOT_URL + "/deling/common/history/delSingleHistoryMusic";
    public final static String UPLOAD_DEVICE_MQ_STATUS = ROOT_URL + "/deling/device/user/upDeviceMqStatus";
    public final static String UPLOAD_ERROR_MSG = ROOT_URL + "/deling/device/upload/reportErrMsg";     //设备端上报错误信息

    public final static String UNBIND_DEVICE = ROOT_URL + "/deling/common/relation/addRelation";//解绑设备

    /**
     * 设备端控制设备端
     */
    public final static String CTL_DEVICE_MUSICPLAY = ROOT_URL + "/deling/device/control/controlDeviceMusicPlay";
    public final static String CTL_DEVICE_MUSIC_PRE_NEXT = ROOT_URL + "/deling/device/control/controlDeviceMusicPreOrNext";
    public final static String CTL_DEVICE_MUSIC_SOURCE = ROOT_URL + "/deling/device/control/controlDeviceMusicSorce";
    public final static String CTL_DEVICE_VOMESIZE = ROOT_URL + "/deling/device/control/controlDeviceMusicVomeSize";
    public final static String CTL_DEVICE_MUSIC_MODEL = ROOT_URL + "/deling/device/control/controlDevicelMusicModel";
    public final static String CTL_DEVICE_ZONE = ROOT_URL + "/deling/device/control/deviceControlDeviceZone";
    public final static String CTL_DEVICE_MIGU_MUSICPLAY = ROOT_URL + "/deling/device/control/devicePlayDeviceMiGuMusic";
    public final static String CTL_DEVICE_MUSIC_INDEX = ROOT_URL + "/deling/device/control/devicePlayDeviceMusic";
    public final static String CTL_DEVICE_LINK = ROOT_URL + "/deling/device/control/deviceControlDeviceZoneLink";
    public final static String CTL_DEVICE_SOUND = ROOT_URL + "/deling/device/control/deviceControlDeviceSound";
    //推送相关
    public final static String UPLOAD_VOICEMEDIA = ROOT_URL + "/deling/common/upload/pushVoiceMedia";
    /**
     * 版本更新相关接口
     */
    public final static String GET_NOW_APK_MESSAGE = ROOT_URL + "/deling/common/apkVersion/getDeviceApkVersion";//获取服务器上面apk的信息

    public final static String DEVICE_UP_AUTH_INFO = ROOT_URL + "/deling/device/tuya/deviceUpAuthInfo";//设备上报酷狗信息
    public final static String DEVICE_PUSH_ACCESSTOKEN = ROOT_URL + "/deling/device/tuya/devicePushAccessToken"; //上传token 跟手机同步
    public final static String DEVICE_PUSH_SWITCHKUGOUUSER = ROOT_URL + "/deling/device/upload/switchKuGouUser"; //酷狗登录成功上报

    public final static String DEVICE_UNLOCKVIPSTATUS = ROOT_URL + "/deling/device/tuya/unLockVipStatus"; //解锁VIP，让音乐可用
    public final static String DEVICE_GETUNLOCKVIPSTATUS = ROOT_URL + "/deling/device/tuya/getUnLockVipStatus"; //获取音乐服务剩余时间

    public final static String WISE_GET_QUERY = ROOT_URL + "/deling/common/pay/wx/getOrderList";//价目表
    public final static String WISE_GET_WXPAYCODE = ROOT_URL + "/deling/common/pay/wx/order";//微信二维码
    public final static String WISE_NOTIFY = ROOT_URL + "/deling/common/pay/wx/notify";//支付通知
    public final static String WISE_GET_ORDERSTATUS = ROOT_URL + "/deling/common/pay/wx/getOrderStatus";//订单状态
    public final static String GET_APKARC_DOWNURL = ROOT_URL + "/deling/common/apkVersion/getMasterApkQrc";     //获取服务器上面 手机端二维码下载地址。Android/IOS  下载的二维码地址

    public final static String MIGU_GET_ORDERSSTATUS = ROOT_URL + "/deling/common/pay/wx/order";//续费微信二维码状态
    public final static String MIGU_PAY_COMBO = ROOT_URL + "/deling/common/pay/wx/getOrderList";//查询当前音乐付费套餐信息

    public final static String AI_STATE_URL = ROOT_URL + "/deling/vas/vasActivation/checkProductActivationState";//AI授权状态
    public final static String AI_AUTHORIZATION_URL = ROOT_URL + "/deling/vas/vasActivation/applyForActivation";//AI申请授权
    public final static String AI_PAY_URL = ROOT_URL + "/deling/pay/unifiedOrder";//AI支付
    public final static String AI_PRODUCT_URL = ROOT_URL + "/deling/vas/vasProduct/queryProductInformation";//AI产品信息
    public final static String AI_PAYSTATE_URL = ROOT_URL + "/deling/vas/order/queryPaymentStatus/";//AI支付状态
    public final static String GET_MIOT_AUTH = ROOT_URL+"/deling/device/miot/getMiotAuth";//获取米家三元组
    public final static String REPORT_DEVICE_INFORMATION = ROOT_URL+"/deling/device/collect/uploadDeviceInformation";//上报设备信息
    public final static String REPORT_VIP_RECEIVE = ROOT_URL+"/deling/device/migu/record";//上报咪咕领取vip接口

}
