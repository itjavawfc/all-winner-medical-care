package com.wanxiang.commonlib.network;

public interface INetEvent {
    void onNetChange(int netWorkState);
}