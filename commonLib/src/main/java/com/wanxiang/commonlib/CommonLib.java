package com.wanxiang.commonlib;

import android.content.Context;
import android.os.Handler;

//import com.balysv.materialripple.MaterialRippleLayout;
//import com.blankj.utilcode.util.SPUtils;

import java.util.HashMap;
import java.util.Map;


public class CommonLib {

    private static Context mContext;
    private static Handler mqAsynHandler;
    private static Handler asynHandler;
    public static String  deviceId;
    //类初始化时，不初始化这个对象(延时加载，真正用的时候再创建)
    private static CommonLib instance;
    private CommonLib(){}
    public static synchronized CommonLib getInstance(){
        if(instance==null){
            instance=new CommonLib();
        }
        return instance;
    }

    public static void init(Context context) {
        mContext = context;
        initMqAsynHandler();
        initAsynHandler();
    }

    public static Context getContext() {
        return mContext;
    }


    private static void initMqAsynHandler() {
        AsynHandler thread = new AsynHandler("MqttThread");
        thread.start();
        mqAsynHandler = new Handler(thread.getLooper());
    }
    private static void initAsynHandler() {
        AsynHandler thread = new AsynHandler("Asys");
        thread.start();
        asynHandler = new Handler(thread.getLooper());
    }
    public static Handler getMqAsynHandler() {
        return mqAsynHandler;
    }

    public static Handler getAsynHandler() {
        return asynHandler;
    }

}
