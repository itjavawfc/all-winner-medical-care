package com.deling.lib_common_ui.player;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import com.deling.lib_common_ui.event.TouchEvent;
import com.wanxiang.commonlib.utils.LogUtils;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.Nullable;

/**
 * Created by lgj
 * 2020/6/29
 **/
public class MainLayout extends RelativeLayout {

    private static final String TAG = "MainLayout";

    private Context mContext;
    public MainLayout(Context context){
        this(context,null);
    }

    public MainLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

    }


    private int ACTION_DOWN;
    private long posX;
    private long posY;
    private long curPosX;
    private long curPosY;


    public int getACTION_DOWN() {
        return ACTION_DOWN;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                LogUtils.Logd(TAG,"=======DOWN=====");
                EventBus.getDefault().post(new TouchEvent());
                posX = (long) ev.getX();
                posY = (long) ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                LogUtils.Logd(TAG,"=======MOVE=====");
                curPosX = (long) ev.getX();
                curPosY = (long) ev.getY();
                break;
            case MotionEvent.ACTION_UP:
                LogUtils.Logd(TAG,"=======UP=====");
                if ((curPosX - posX > 0) && (Math.abs(curPosX - posX) > 25)){
                    Log.v("MainLayout","向左滑动");
                }
                else if ((curPosX - posX < 0) && (Math.abs(curPosX-posX) > 25)){
                    Log.v("MainLayout","向右滑动");
                }
                if ((curPosY - posY > 0) && (Math.abs(curPosY - posY) > 25)){
                    Log.v("MainLayout","向下滑动");
                }
                else if ((curPosY - posY < 0) && (Math.abs(curPosY-posY) > 25)){
                    Log.v("MainLayout","向上滑动");
                }
                break;
        }

        return false;
    }


   /* @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction()==event.ACTION_DOWN){
            LogUtils.Logd(TAG,"=======down=====");
            EventBus.getDefault().post(new TouchEvent());
        }

        return super.onTouchEvent(event);
    }*/
}
