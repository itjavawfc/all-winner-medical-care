package com.deling.lib_common_ui.player;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

/**
 * Created by lgj
 * 2020/6/29
 **/
public class MusicIconLayout extends RelativeLayout {


    private Context mContext;
    public MusicIconLayout(Context context){
        this(context,null);
    }

    public MusicIconLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        return true;
    }
}
