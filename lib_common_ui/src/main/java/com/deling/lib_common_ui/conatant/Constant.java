package com.deling.lib_common_ui.conatant;

import android.Manifest;

/**
 * Created by lgj
 * 2020/4/26
 * @function app常量
 **/
public class Constant {


    /**
     * 权限常量相关
     */
    public static final int WRITE_READ_EXTERNAL_CODE = 0x01;
    public static final String[] WRITE_READ_EXTERNAL_PERMISSION = new String[] {
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static final int KEY_F1 = 0;
    public static final int KEY_F2 = 1;
    public static final int KEY_F3 = 2;
    public static final int KEY_F4 = 3;
    public static final int KEY_F5 = 4;

}
