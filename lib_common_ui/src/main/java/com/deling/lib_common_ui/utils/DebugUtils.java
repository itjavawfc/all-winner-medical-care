package com.deling.lib_common_ui.utils;

import android.util.Log;

import com.wanxiang.commonlib.utils.LogUtils;

/**
 * Created by lgj
 * 2020/6/18
 **/
public class DebugUtils {


    public static void printTime(String TAG,String message){
        String log=(System.currentTimeMillis()+"    "+message +"====lgj====").substring(7);
        LogUtils.Logd(TAG,log);
    }

}
