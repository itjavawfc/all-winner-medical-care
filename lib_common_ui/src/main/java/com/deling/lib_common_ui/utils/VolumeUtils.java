package com.deling.lib_common_ui.utils;

import android.content.Context;
import android.media.AudioManager;

import com.wanxiang.commonlib.utils.LogUtils;

/**
 * Created by lgj
 * 2020/6/28
 **/
public class VolumeUtils {

    private static final String TAG = "VolumeUtils";

    //获取系统音量
    public static int getVolume(Context mContext) {
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        //获取系统音量
        if (am != null) {
            return am.getStreamVolume(AudioManager.STREAM_MUSIC);
        } else return 0;

    }

    /**
     * 设置系统音量
     *
     * @param mContext
     * @param vol
     */
    public static void setVolume(Context mContext, int vol) {
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (am != null) {
            LogUtils.Logd(TAG,"begin setStreamVolume===="+vol);
            am.setStreamVolume(AudioManager.STREAM_MUSIC, vol, 0);
            LogUtils.Logd(TAG,"end setStreamVolume===="+vol);
            if(vol>0) {
                muteState = false;
            }
        }
    }


    private static boolean muteState=false;//静音关闭
    private static int lastSystemVolume=0;//静音前音量

    //设置静音
    public static void setMute(Context mContext, boolean isMute) {
        if(isMute){
            muteState=true;
            lastSystemVolume=getVolume(mContext);
            setVolume(mContext,0);
        }else{
            muteState=false;
            setVolume(mContext,lastSystemVolume);
        }
        AudioManager mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        assert mAudioManager != null;
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, isMute);

    }

    /**
     * 判断当前是否静音
     * @return
     */
    public static boolean isMute(){
        return muteState;
    }

    private static int calcOneVolume(int vol) {
        int volume = 0;
        switch (vol) {
            case 0:
                volume = 0;
                break;
            case 1:
                volume = 4;
                break;
            case 2:
                volume = 5;
                break;
            case 3:
                volume = 6;
                break;
            case 4:
                volume = 9;
                break;
            case 5:
                volume = 11;
                break;
            case 6:
                volume = 12;
                break;
            case 7:
                volume = 14;
                break;
            case 8:
                volume = 15;
                break;
            case 9:
                volume = 16;
                break;
            case 10:
                volume = 17;
                break;
            case 11:
                volume = 18;
                break;
            case 12:
                volume = 19;
                break;
            case 13:
                volume = 20;
                break;
            case 14:
                volume = 21;
                break;
            case 15:
                volume = 22;
                break;
            default:

                break;
        }
        return volume;
    }



    private static int calcTwoVolume(int vol) {
        int volume = 0;
        switch (vol) {
            case 0:
                volume = 255;
                break;
            case 1:
                volume = 84;
                break;
            case 2:
                volume = 80;
                break;
            case 3:
                volume = 77;
                break;
            case 4:
                volume = 65;
                break;
            case 5:
                volume = 57;
                break;
            case 6:
                volume = 52;
                break;
            case 7:
                volume = 45;
                break;
            case 8:
                volume = 41;
                break;
            case 9:
                volume = 37;
                break;
            case 10:
                volume = 33;
                break;
            case 11:
                volume = 30;
                break;
            case 12:
                volume = 26;
                break;
            case 13:
                volume = 21;
                break;
            case 14:
                volume = 17;
                break;
            case 15:
                volume = 13;
                break;
            default:

                break;
        }
        return volume;
    }


    public static int getNoticeVolume(Context mContext){
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (am != null) {
            return am.getStreamVolume(AudioManager.STREAM_SYSTEM);
        } else return 0;
    }

    public static void setNoticeVolume(Context mContext,int volume){
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (am != null) {
            am.setStreamVolume(AudioManager.STREAM_SYSTEM, volume, 0);
        }
    }
}
