package com.deling.lib_common_ui.event;

/**
 * Created by Changer on 2021/12/2
 */
public class ControlDeviceEvent {
    int type;//0进度加

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ControlDeviceEvent(int type) {
        this.type = type;
    }
}
