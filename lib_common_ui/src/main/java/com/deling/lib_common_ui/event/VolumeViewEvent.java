package com.deling.lib_common_ui.event;

/**
 * Created by lgj
 * 2020/7/14
 **/
public class VolumeViewEvent {
    public boolean isVolumeShow=false;
    public boolean isUp = false;

    public VolumeViewEvent(){

    }
    public VolumeViewEvent(boolean isVolumeShow){
        this.isVolumeShow=isVolumeShow;
    }

    public boolean isVolumeShow() {
        return isVolumeShow;
    }

    public void setVolumeShow(boolean volumeShow) {
        isVolumeShow = volumeShow;
    }

    public boolean isUp() {
        return isUp;
    }

    public void setUp(boolean up) {
        isUp = up;
    }
}
