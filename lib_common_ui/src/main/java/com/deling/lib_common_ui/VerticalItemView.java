package com.deling.lib_common_ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;


/**
 * 垂直icon+msg
 */
public class VerticalItemView extends RelativeLayout {

    private Context mContext;

    /*
     * 所有样式属性
     */
    private int mIconWidth;
    private int mIconHeight;
    private Drawable mIcon;

    private int mTipPaddingTop;
    private int mTipPaddingRight;
    private Drawable mTipBg;
    private int mTipTextColor;
    private float mTipTextSize;
    private String mTipText;

    private float mInfoTextSize;
    private int mInfoTextColor;
    private int mInfoTextMarginTop;
    private String mInfoText;

    /*
     * 所有View
     */
    private ImageView mIconView;
    private TextView mTipView;
    private TextView mInfoView;
    private int infoMaxLines;
//    private TextUtils.TruncateAt infoEllipsize;


//    private BaseEffectsProxy mEffectsProxy;

    public VerticalItemView(Context context){
        this(context,null);
    }

    public VerticalItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.VerticalItem);
        mIconWidth = a.getLayoutDimension(R.styleable.VerticalItem_iconWidth, 35);
        mIconHeight = a.getLayoutDimension(R.styleable.VerticalItem_iconHeight, 35);
        mIcon = a.getDrawable(R.styleable.VerticalItem_icon);
        mTipPaddingTop = a.getLayoutDimension(R.styleable.VerticalItem_tipPaddingTop, 2);
        mTipPaddingRight = a.getLayoutDimension(R.styleable.VerticalItem_tipPaddingRight, 2);
        mTipBg = a.getDrawable(R.styleable.VerticalItem_tipBg);
        mTipTextColor = a.getColor(R.styleable.VerticalItem_tipTextColor, 0xffffff);
        mTipTextSize = a.getDimension(R.styleable.VerticalItem_tipTextSize, 12);
        mTipText = a.getString(R.styleable.VerticalItem_tipText);
        mInfoTextSize = a.getDimension(R.styleable.VerticalItem_infoTextSize, 12);
        mInfoTextColor = a.getColor(R.styleable.VerticalItem_infoTextColor, 0x333333);
        mInfoTextMarginTop = a.getLayoutDimension(R.styleable.VerticalItem_infoTextMarginTop, 10);
        mInfoText = a.getString(R.styleable.VerticalItem_infoText);
        infoMaxLines = a.getInteger(R.styleable.VerticalItem_infoMaxLines, 1);
        a.recycle();

        //居中添加到布局中
        LayoutParams params =
                new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(CENTER_IN_PARENT);
        addView(createItemView(), params);



/*
        EffectsAdapter adapter = null;

            adapter = new TouchScaleAdapter(TouchEffectsManager.getScaleBean());

        if(adapter == null){
            return ;
        }

        BaseEffectsProxy baseEffectsProxy;
        if(TypeUtils.isContainsExtraType(TouchEffectsExtraType.AspectRatio)){
            baseEffectsProxy = new AspectRatioEffectsProxy(adapter);
        }else{
            baseEffectsProxy = new BaseEffectsProxy(adapter);
        }
        mEffectsProxy = baseEffectsProxy;
        mEffectsProxy.initAttr(context,attrs);*/
    }
    //TODO 需要增加代码设置文字和图片功能

    public void setmInfoText(String mInfoText){
        mInfoView.setText(mInfoText);
    }
    public void setmInfoIcon(Drawable mIcon){
        mIconView.setImageDrawable(mIcon);
    }



    /**
     * 构建自己的组合view
     */
    private View createItemView() {
        RelativeLayout rootLayout = new RelativeLayout(mContext);
        mIconView = new ImageView(mContext);
        mIconView.setImageDrawable(mIcon);
        mIconView.setId(R.id.vertical_image_id);
        LayoutParams iconParams =
                new LayoutParams(mIconWidth, mIconHeight);
        iconParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        rootLayout.addView(mIconView, iconParams);

        mInfoView = new TextView(mContext);
        mInfoView.setId(R.id.vertical_text_id);
        mInfoView.setTextColor(mInfoTextColor);
        mInfoView.getPaint().setTextSize(mInfoTextSize);
        mInfoView.setText(mInfoText);
        mInfoView.setMaxLines(infoMaxLines);
//        mInfoView.setEllipsize(infoEllipsize);

        LayoutParams textParams =
                new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textParams.setMargins(0, mInfoTextMarginTop, 0, 0);
        textParams.addRule(RelativeLayout.BELOW, R.id.vertical_image_id);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        rootLayout.addView(mInfoView, textParams);

        //mTipView = new TextView(mContext);
        //mTipView.setId(R.id.vertical_tip_id);
        //mTipView.setBackground(mTipBg);
        //mTipView.setText(mTipText);
        //mTipView.getPaint().setTextSize(mTipTextSize);
        //mTipView.setTextColor(mTipTextColor);
        //RelativeLayout.LayoutParams tipParams =
        //    new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        //tipParams.addRule(RelativeLayout.RIGHT_OF, R.id.vertical_image_id);
        //tipParams.addRule(RelativeLayout.ABOVE, R.id.vertical_image_id);
        //tipParams.setMargins(0, mTipPaddingTop, mTipPaddingRight, 0);
        //rootLayout.addView(mTipView, tipParams);
        return rootLayout;
    }


  /*  @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mEffectsProxy.measuredSize(getMeasuredWidth(),getMeasuredHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mEffectsProxy.getAdapter().runAnimator(this,canvas);
        super.onDraw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            mEffectsProxy.getAdapter().dispatchDraw(this,canvas);
        }
    }

    @Override
    public void onDrawForeground(Canvas canvas) {
        super.onDrawForeground(canvas);
        mEffectsProxy.getAdapter().drawForeground(this,canvas);
    }

    Animator mEngineAnimator;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(mOnClickListener == null && mOnLongClickListener == null || !isEnabled()){
            return super.onTouchEvent(event);
        }
        return mEffectsProxy.getAdapter().onTouch(this,event,mOnClickListener,mOnLongClickListener);
    }

    private float mShakeScale = 0.85f;
    private float mCurrentScaleX = 1.0f,mCurrentScaleY = 1.0f;

    int mAnimationDuration = 100;
    protected Animator createEngineAnimator(View view) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(1.0f, mShakeScale);
        valueAnimator.setDuration(mAnimationDuration);
        valueAnimator.addUpdateListener(animation -> {
            mCurrentScaleX = (float) animation.getAnimatedValue();
            mCurrentScaleY = mCurrentScaleX;
            view.invalidate();
        });
        return valueAnimator;
    }



    public OnClickListener mOnClickListener;
    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        mOnClickListener = l;
    }

    public OnLongClickListener mOnLongClickListener;
    @Override
    public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
        mOnLongClickListener = onLongClickListener;
        if(mOnLongClickListener != null){
            mEffectsProxy.getAdapter().createLongClick(this,mOnLongClickListener);
        }
    }*/
}

