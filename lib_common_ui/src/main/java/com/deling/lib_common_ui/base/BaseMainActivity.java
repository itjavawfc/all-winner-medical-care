package com.deling.lib_common_ui.base;


import android.app.Instrumentation;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.deling.lib_common_ui.conatant.Constant;
import com.deling.lib_common_ui.utils.VolumeUtils;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.utils.LogUtils;
import com.wanxiang.commonlib.utils.ThreadPoolUtils;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;


/**
 * Created by lgj
 * 2020/6/17
 **/
public class BaseMainActivity extends FragmentActivity {

    private static final String TAG = "BaseMainActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        TouchEffectsFactory.initTouchEffects(this);
        super.onCreate(savedInstanceState);
        if(getRequestedOrientation()!=ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        hideBottomUIMenu();
    }

    private void hideBottomUIMenu() {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // 状态栏（以上几行代码必须，参考setStatusBarColor|setNavigationBarColor方法源码）
            window.setStatusBarColor(Color.TRANSPARENT);
            // 虚拟导航键
            window.setNavigationBarColor(Color.TRANSPARENT);
    }

    /**
     * 申请指定的权限.
     */
    public void requestPermission(int code, String... permissions) {
        ActivityCompat.requestPermissions(this, permissions, code);
    }

    /**
     * 判断是否有指定的权限
     */
    public boolean hasPermission(String... permissions) {

        for (String permisson : permissions) {
            if (ContextCompat.checkSelfPermission(this, permisson) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

       @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 处理整个应用用中的读写文件操作业务
     */
    public void doWriteOrReadPermission() {

    }

   /* @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(0,0);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }*/

    int upVolume = 0;
    int downVolume = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        LogUtils.Logd(TAG,"BaseMainActivity===>keyCode===>"+keyCode);
        if(keyCode==KeyEvent.KEYCODE_VOLUME_DOWN){
            LogUtils.Logd(TAG,"KEYCODE_VOLUME==="+keyCode);
            int vol = VolumeUtils.getVolume(CommonLib.getContext());
            LogUtils.Logd(TAG,"vol===>"+vol);
                if (vol > 0){
                    vol = vol - 1;
                }
            VolumeUtils.setVolume(CommonLib.getContext(),vol);
            return true;
        }else if(keyCode==KeyEvent.KEYCODE_VOLUME_UP){
            LogUtils.Logd(TAG,"KEYCODE_VOLUME==="+keyCode);
            int vol = VolumeUtils.getVolume(CommonLib.getContext());
                if (vol < 15){
                    vol = vol + 1;
                }
            VolumeUtils.setVolume(this,vol);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
