package com.deling.lib_network.response;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.deling.lib_network.exception.OkHttpException;
import com.deling.lib_network.listener.DisposeDataHandle;
import com.deling.lib_network.listener.DisposeDataListener;
import com.google.gson.Gson;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.bean.callbackbean.ErrorCallbackBean;
import com.wanxiang.commonlib.utils.LogUtils;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * @author vision
 * @function 专门处理JSON的回调
 */
public class CommonJsonCallback implements Callback {

    private static final String TAG = "CommonJsonCallback";
    /**
     * the logic layer exception, may alter in different app
     */
    protected final String RESULT_CODE = "ecode"; // 有返回则对于http请求来说是成功的，但还有可能是业务逻辑上的错误
    protected final int RESULT_CODE_VALUE = 0;
    protected final String ERROR_MSG = "emsg";
    protected final String EMPTY_MSG = "";

    /**
     * the java layer exception, do not same to the logic error
     */
    protected final int NETWORK_ERROR = -1; // the network relative error
    protected final int JSON_ERROR = -2; // the JSON relative error
    protected final int OTHER_ERROR = -3; // the unknow error

    /**
     * 将其它线程的数据转发到UI线程
     */
    private Handler mDeliveryHandler;
    private DisposeDataListener mListener;
    private Class<?> mClass;

    public CommonJsonCallback(DisposeDataHandle handle) {
        this.mListener = handle.mListener;
        this.mClass = handle.mClass;
        this.mDeliveryHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onFailure(final Call call, final IOException ioexception) {
        LogUtils.Logd(TAG,"onFailure ===> "+call.toString()+" io => "+ioexception.getMessage());
        /**
         * 此时还在非UI线程，因此要转发
         */
        mDeliveryHandler.post(new Runnable() {
            @Override
            public void run() {
                mListener.onFailure(new OkHttpException(NETWORK_ERROR, ioexception));
            }
        });
    }

    @Override
    public void onResponse(final Call call, final Response response) throws IOException {
        final String result = response.body().string();
        LogUtils.Logd(TAG,"result ===> "+result);
        if (result.contains("没有绑定关系")){
        }
        try {
            ErrorCallbackBean bean = new Gson().fromJson(result,ErrorCallbackBean.class);
            if (bean.getCode() != null && !bean.getCode().equals("")
                    && !bean.getCode().isEmpty()){
                if (bean.getCode().equals("10002") && bean.getMsg() != null && !bean.getMsg().equals("")
                        && !bean.getMsg().isEmpty()){
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /**
                             * 延时执行的代码
                             */
                            Toast.makeText(CommonLib.getContext(),bean.getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    },1000); // 延时1秒
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        mDeliveryHandler.post(new Runnable() {
            @Override
            public void run() {
                handleResponse(result);
            }
        });
    }

    private void handleResponse(Object responseObj) {
        if (responseObj == null || responseObj.toString().trim().equals("")) {
            mListener.onFailure(new OkHttpException(NETWORK_ERROR, EMPTY_MSG));
            return;
        }
//        LogUtils.Logd(TAG,"H6","reponse=="+responseObj.toString());

        try {
            /**
             * 协议确定后看这里如何修改
             */

            if (mClass == null) {
//                JSONObject result = new JSONObject(responseObj.toString()); //todo
                mListener.onSuccess(responseObj);
            } else {
                Object obj = new Gson().fromJson(responseObj.toString(), mClass);
                if (obj != null) {
                    mListener.onSuccess(obj);
                } else {
                    mListener.onFailure(new OkHttpException(JSON_ERROR, EMPTY_MSG));
                }
            }
        } catch (Exception e) {
            mListener.onFailure(new OkHttpException(OTHER_ERROR, e.getMessage()));
            e.printStackTrace();
        }
    }
}