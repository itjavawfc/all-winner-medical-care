package com.deling.lib_network.response;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.wanxiang.commonlib.utils.LogUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class CommomCallback implements Callback {
    private static final String TAG = "CommomCallback";
    private Handler mHandler;
    private IResponseHandler mResponseHandler;

    public CommomCallback( IResponseHandler responseHandler) {
        this.mHandler = new Handler(Looper.getMainLooper());
        mResponseHandler = responseHandler;
    }

    @Override
    public void onFailure(final Call call, final IOException e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if(call.isCanceled() || e.toString().contains("closed"))return;
                mResponseHandler.onFailure(0, e.toString());
            }
        });
    }

    @Override
    public void onResponse(Call call, final Response response) throws IOException {
        if(response.isSuccessful()) {
            final String response_body = response.body().string();
            if(mResponseHandler instanceof JsonResponseHandler) {       //json回调
                try {
                    final JSONObject jsonBody = new JSONObject(response_body);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            ((JsonResponseHandler)mResponseHandler).onSuccess(response.code(), jsonBody);
                        }
                    });
                } catch (JSONException e) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mResponseHandler.onFailure(response.code(), "fail parse jsonobject, body=" + response_body);
                        }
                    });
                }
            } else if(mResponseHandler instanceof GsonResponseHandler) {    //gson回调
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Gson gson = new Gson();
                            ((GsonResponseHandler)mResponseHandler).onSuccess(response.code(),
                                    gson.fromJson(response_body, ((GsonResponseHandler)mResponseHandler).getType()));
                        } catch (Exception e) {
                            mResponseHandler.onFailure(response.code(), "fail parse gson, body=" + response_body);
                        }

                    }
                });
            } else if(mResponseHandler instanceof RawResponseHandler) {     //raw字符串回调
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        ((RawResponseHandler)mResponseHandler).onSuccess(response.code(), response_body);
                    }
                });
            }
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mResponseHandler.onFailure(0, "fail status=" + response.code());
                }
            });
        }
    }
}
