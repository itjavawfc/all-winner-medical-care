package com.deling.lib_network.model.event;

public class PayOrderNoEvent {
    private String orderNo;
    private int code;//0,执行 1，取消

    public PayOrderNoEvent(String orderNo, int code) {
        this.orderNo = orderNo;
        this.code = code;
    }

    public PayOrderNoEvent(int code) {
        this.code = code;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
