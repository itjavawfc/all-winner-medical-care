package com.deling.lib_network.model;

public class UserInfo {

    /**
     * userId : string
     * nickName : string
     * img : string
     * isVip : 0
     * vipEndTime : 2020-01-01 00:00:00
     */

    private String userId;
    private String nickName;
    private String img;
    private int isVip;
    private String vipEndTime;

    @Override
    public String toString() {
        return "UserInfo{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", img='" + img + '\'' +
                ", isVip=" + isVip +
                ", vipEndTime='" + vipEndTime + '\'' +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getIsVip() {
        return isVip;
    }

    public void setIsVip(int isVip) {
        this.isVip = isVip;
    }

    public String getVipEndTime() {
        return vipEndTime;
    }

    public void setVipEndTime(String vipEndTime) {
        this.vipEndTime = vipEndTime;
    }
}
