package com.deling.lib_network.model;

public class Upload extends BaseModel{

    private String sp;
    private String userId;
    private String songId;
    private int duration;
    private int playTime;
    private int playType;
    private String api;
    private String sourceId;
    private String lvt;
    private String clientIp;

    private int tryTime;

    public int getTryTime() {
        return tryTime;
    }

    public void setTryTime(int tryTime) {
        this.tryTime = tryTime;
    }

    @Override
    public String toString() {
        return "Upload{" +
                "sp='" + sp + '\'' +
                ", userId='" + userId + '\'' +
                ", songId='" + songId + '\'' +
                ", duration=" + duration +
                ", playTime=" + playTime +
                ", playType=" + playType +
                ", api='" + api + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", lvt='" + lvt + '\'' +
                ", clientIp='" + clientIp + '\'' +
                ", tryTime=" + tryTime +
                '}';
    }

    public String getSp() {
        return sp;
    }

    public void setSp(String sp) {
        this.sp = sp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPlayTime() {
        return playTime;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public int getPlayType() {
        return playType;
    }

    public void setPlayType(int playType) {
        this.playType = playType;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getLvt() {
        return lvt;
    }

    public void setLvt(String lvt) {
        this.lvt = lvt;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
}
