package com.deling.lib_network.request;

import com.wanxiang.commonlib.utils.LogUtils;

import java.io.File;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * @author vision
 * @function build the request
 */
public class CommonRequest {

    private static final String TAG = "CommonRequest";
    /**
     * create the key-value Request
     *
     * @param url
     * @param params
     * @return
     */
    public static Request createPostRequest(String url, RequestParams params) {
        return createPostRequest(url, params, null);
    }

    /**
     * 可以带请求头的Post请求
     *
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public static Request createPostRequest(String url, RequestParams params, RequestParams headers) {
        FormBody.Builder mFormBodyBuild = new FormBody.Builder();
        if (params != null) {
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
                mFormBodyBuild.add(entry.getKey(), entry.getValue());
            }
        }
        //添加请求头
        Headers.Builder mHeaderBuild = new Headers.Builder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.urlParams.entrySet()) {
                mHeaderBuild.add(entry.getKey(), entry.getValue());
            }
        }
        FormBody mFormBody = mFormBodyBuild.build();
        Headers mHeader = mHeaderBuild.build();
        Request request = new Request.Builder().url(url).
                post(mFormBody).
                headers(mHeader)
                .build();
        return request;
    }

    /**
     * ressemble the params to the url
     *
     * @param url
     * @param params
     * @return
     */
    public static Request createGetRequest(String url, RequestParams params) {

        return createGetRequest(url, params, null);
    }

    /**
     * 可以带请求头的Get请求
     *
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public static Request createGetRequest(String url, RequestParams params, RequestParams headers) {
        StringBuilder urlBuilder = new StringBuilder(url).append("?");
        if (params != null) {
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
                urlBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        //添加请求头
        Headers.Builder mHeaderBuild = new Headers.Builder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.urlParams.entrySet()) {
                mHeaderBuild.add(entry.getKey(), entry.getValue());
            }
        }
        Headers mHeader = mHeaderBuild.build();
        return new Request.Builder().
                url(urlBuilder.substring(0, urlBuilder.length() - 1))
                .get()
                .headers(mHeader)
                .build();
    }


    /**
     * post 请求,不带请求头
     * @param url
     * @param params json格式
     * @return
     */
    public static Request createPostRequest(String url, String params){
        return createPostRequest(url,params,null);
    }

    /**
     * 图片上传
     * */
    public static Request createPostFileRequest(String url, RequestParams params){
        return createMultiPostRequest(url,params);
    }

    /**
     * 语音文件上传
     */
    public static Request createPostVoiceFileRequest(String url, RequestParams params){
        return createMultiVoicePostRequest(url,params);
    }

    /**
     * 文件下下载
     * */
    public static Request createPostFileDownLoadRequest(String url, String savePath){
        return createPostFileDownLoadRequest(url);
    }

    private static final MediaType JSON=MediaType.parse("application/json; charset=utf-8");

    /**
     * 可以带请求头的Post请求
     *
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public static Request createPostRequest(String url, String params, RequestParams headers) {
        RequestBody mFormBody=new FormBody.Builder().build();
        if (params != null) {
            mFormBody = RequestBody.create(JSON,params);
        }
        //添加请求头
        Headers.Builder mHeaderBuild = new Headers.Builder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.urlParams.entrySet()) {
                mHeaderBuild.add(entry.getKey(), entry.getValue());
            }
        }

        Headers mHeader = mHeaderBuild.build();
        LogUtils.Logd(TAG,"header ===> "+mHeader.toString());
        Request request = new Request.Builder().url(url).
                post(mFormBody).
                headers(mHeader).
                tag(url)
                .build();
        return request;
    }

    public static Request createPostRequestTag(String tag,String url, String params, RequestParams headers) {
        RequestBody mFormBody=new FormBody.Builder().build();
        if (params != null) {
            mFormBody = RequestBody.create(JSON,params);
        }
        //添加请求头
        Headers.Builder mHeaderBuild = new Headers.Builder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.urlParams.entrySet()) {
                mHeaderBuild.add(entry.getKey(), entry.getValue());
            }
        }

        Headers mHeader = mHeaderBuild.build();
        LogUtils.Logd(TAG,"header ===> "+mHeader.toString());
        Request request = new Request.Builder().url(url)
                .post(mFormBody)
                .headers(mHeader)
                .tag(tag)
                .build();
        return request;
    }

    /**
     * 文件上传请求
     *
     * @return
     */
//    private static final MediaType FILE_TYPE = MediaType.parse("application/octet-stream");
    private static final MediaType FILE_TYPE = MediaType.parse("multipart/form-data");

    public static Request createMultiPostRequest(String url, RequestParams params) {
        MultipartBody.Builder requestBody = new MultipartBody.Builder();
        requestBody.setType(MultipartBody.FORM);
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.fileParams.entrySet()) {
                if (entry.getValue() instanceof File[]) {
                    File[] files= (File[]) entry.getValue();
                  for(File file:files){
                      requestBody.addFormDataPart("musicPic", file.getName(),
                              RequestBody.create(MediaType.parse("image/jpg"), file));
                  }
                } else if (entry.getValue() instanceof String) {
                    requestBody.addFormDataPart(entry.getKey(), (String) entry.getValue());
                } else if(entry.getValue() instanceof String[]){
                    String[] musicIds= (String[]) entry.getValue();
                    for(String musidId:musicIds){
                        requestBody.addFormDataPart("musicId",musidId);
                    }
                }
            }
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
               if (entry.getValue() instanceof String) {
                /*    requestBody.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + entry.getKey() + "\""),
                            RequestBody.create(null, (String) entry.getValue()));*/
                    requestBody.addFormDataPart(entry.getKey(), (String) entry.getValue());
                }
            }


        }
        return new Request.Builder().url(url).post(requestBody.build()).build();
    }

    public static Request createMultiVoicePostRequest(String url, RequestParams params) {
        MultipartBody.Builder requestBody = new MultipartBody.Builder();
        requestBody.setType(MultipartBody.FORM);
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.fileParams.entrySet()) {
                if (entry.getValue() instanceof File[]) {
                    File[] files= (File[]) entry.getValue();
                    for(File file:files){
                        requestBody.addFormDataPart("mediaSource", file.getName(),
                                RequestBody.create(MediaType.parse("audio/mp3"), file));
                    }
                } else if (entry.getValue() instanceof String) {
                    requestBody.addFormDataPart(entry.getKey(), (String) entry.getValue());
                } else if(entry.getValue() instanceof String[]){
                    String[] deviceuids= (String[]) entry.getValue();
                    for(String deviceuid:deviceuids){
                        requestBody.addFormDataPart("deviceuids",deviceuid);
                    }
                }
            }
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
                if (entry.getValue() instanceof String) {
                /*    requestBody.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + entry.getKey() + "\""),
                            RequestBody.create(null, (String) entry.getValue()));*/
                    requestBody.addFormDataPart(entry.getKey(), (String) entry.getValue());
                }
            }


        }
        return new Request.Builder().url(url).post(requestBody.build()).build();
    }

    /**
     * 文件下载请求
     * */
    public static Request createPostFileDownLoadRequest(String url) {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Connection", "close")    //这里不设置可能产生EOF错误
                .build();
        return request;
    }

 /*   *//**
     * 文件上传请求
     *
     * @return
     *//*
    private static final MediaType FILE_TYPE_PARAMs = MediaType.parse("multipart/form-data");
*/

}