package com.deling.launcher;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.deling.launcher.application.MyApplication;
import com.deling.lib_common_ui.base.BaseMainActivity;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.utils.LogUtils;
import com.wanxiang.commonlib.utils.PropertiesUtils;
public class VideoDoctorActivity extends BaseMainActivity implements // BottomAppAdapter.OnItemClickLitener,
        View.OnClickListener {//IMqttMsgReceiveListener , INetEvent

 /*   private Context mContext;
    private RelativeLayout main_title_layout;
     ImageView mainBluetoothIcon;*/
//    ImageView mainWifiIcon;



//    private boolean isFirstInit = false;

    private ImageView familyVersion,businessVersion;
     private static final String TAG = "MainActivity";


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().getDecorView().setBackground(null);
        setContentView(R.layout.activity_videodoctor);

        stopAnimation();

 familyVersion=findViewById(R.id.family_version);
        businessVersion=findViewById(R.id.business_version);
   familyVersion.setOnClickListener(this);
   businessVersion.setOnClickListener(this);

         initData();
        initBroadCast();
     }







    @Override
    protected void onResume() {
        super.onResume();
        canClick = true;

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initBroadCast() {
     }


/*

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogUtils.Logd(TAG, "onTouchEvent: ");
        return super.onTouchEvent(event);
    }

*/



    private void initData() {
        LogUtils.d("MainActivity  initData");

    }


    @Override
    protected void onPause() {
        super.onPause();
        canClick = false;
//        DebugUtils.printTime(TAG, "onPause end");
    }

    @Override
    protected void onStop() {
        super.onStop();
        canClick = false;
//        DebugUtils.printTime(TAG, "onStop end");
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {

        }
    }


    @Override
    public void onClick(View v) {
     /*   familyVersion=findViewById(R.id.family_version);
        businessVersion=findViewById(R.id.business_version);
        businessVersion;
        family_version  business_version*/
        if(v.getId()==R.id.family_version){
            LogUtils.Logd(TAG," 家庭版");
            //AppUtils.launchApp("com.hhmedic.app.patient");
            PackageManager packageManager = CommonLib.getContext().getPackageManager();
            Intent intent=packageManager.getLaunchIntentForPackage("com.hhmedic.app.patient");
            if(intent==null){
                Toast.makeText(MyApplication.getmContext(), "未安装该应用", Toast.LENGTH_SHORT).show();
            }else{
                CommonLib.getContext().startActivity(intent);
            }

        }else if(v.getId()==R.id.business_version){ //机构版本
            LogUtils.Logd(TAG," 机构版本");
           // AppUtils.launchApp("com.hhmedic.app.patient.tv.common.portrait");
            PackageManager packageManager = CommonLib.getContext().getPackageManager();
            Intent intent=packageManager.getLaunchIntentForPackage("com.hhmedic.app.patient.tv.common.portrait");
            if(intent==null){
                Toast.makeText(MyApplication.getmContext(), "未安装该应用", Toast.LENGTH_SHORT).show();
            }else{
                CommonLib.getContext().startActivity(intent);
            }


        }

     }


/*
    @SuppressLint("RestrictedApi")
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            return true;
        } else
            return super.dispatchKeyEvent(event);
    }
*/



    private boolean canClick = true;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        canClick = false;
     }






    private int getStrength(Context context) {
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        if (info.getBSSID() != null) {
            int strength = WifiManager.calculateSignalLevel(info.getRssi(), 5);
            return strength;
        }
        return 0;
    }

    private void initWifiState() {
        updateWifiStrength();
    }

    private void updateWifiStrength() {

        int strength = getStrength(this);
        if (strength > 0) {
            //mainWifiIcon.setImageResource(wifiStateImgs[strength - 1]);
        } else {
          //  mainWifiIcon.setImageResource(R.drawable.wifi_1);
        }
    }

/*    private int[] wifiStateImgs = new int[]{R.drawable.wifi_1,
            R.drawable.wifi_2, R.drawable.wifi_3,
            R.drawable.wifi_4};*/


    @Override
    public void finish() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
        overridePendingTransition(0, 0);
    }

    private void stopAnimation() {
        CommonLib.getAsynHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PropertiesUtils.set("service.bootanim.exit", "1");
            }
        }, 38 * 1000);
    }

}

