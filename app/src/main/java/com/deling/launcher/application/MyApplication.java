package com.deling.launcher.application;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.Utils;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.base.MConstants;
import com.wanxiang.commonlib.utils.NetUtils;
import com.wanxiang.commonlib.utils.ShellUtils;
import androidx.annotation.RequiresApi;
import androidx.multidex.MultiDex;
public class MyApplication extends Application {

    private static MyApplication mYApplication = null;
    private static Context mContext;
    private static final String TAG = "MyApplication";

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        mYApplication = this;
        mContext = this;
        CommonLib.init(getApplicationContext());
        Utils.init(this);
        String processAppName = ShellUtils.getProcessName();
        if (processAppName == null) return;
        NetworkUtils.setWifiEnabled(true);


    }






    public static Context getmContext() {
        return mContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
     }


    public static MyApplication getInstance() {
        return mYApplication;
    }


}
