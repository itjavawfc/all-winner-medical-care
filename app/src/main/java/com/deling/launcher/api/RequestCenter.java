package com.deling.launcher.api;


import com.wanxiang.commonlib.utils.LogUtils;
import com.deling.lib_network.CommonOkHttpClient;
import com.deling.lib_network.listener.DisposeDataHandle;
import com.deling.lib_network.listener.DisposeDataListener;
import com.deling.lib_network.request.CommonRequest;
import com.deling.lib_network.request.RequestParams;

/**
 * 请求中心
 */
public class RequestCenter {

    private static final String TAG = "RequestCenter";
    public static final int HTTP_OK=200;




    //可以根据参数发送所有get请求,自动解析json
    public static void getRequest(String url, RequestParams params, DisposeDataListener listener,
                                  Class<?> clazz) {
        LogUtils.Logd(TAG,"==getRequest==");
        LogUtils.Logd(TAG,"url=="+url);
        CommonOkHttpClient.get(CommonRequest.
                createGetRequest(url, params), new DisposeDataHandle(listener, clazz));
    }


    public static void postRequest(String url, String params, DisposeDataListener listener, Class<?> clazz) {
//        LogUtils.Logd(TAG,"==postRequest==");
        LogUtils.Logd(TAG,"url=="+url);
//        LogUtils.Logd(TAG,"params=="+params);
        CommonOkHttpClient.get(CommonRequest.
                createPostRequest(url, params), new DisposeDataHandle(listener, clazz));
    }


    public static void postFileRequest(String url, RequestParams params, DisposeDataListener listener, Class<?> clazz) {

      //  MultipartBody.Builder builder  RequestParams

        CommonOkHttpClient.get(CommonRequest.
                createPostFileRequest(url, params), new DisposeDataHandle(listener, clazz));

    }



    public static void postParamsRequest(String url,RequestParams requestParams, String params, DisposeDataListener listener, Class<?> clazz){
        LogUtils.Logd(TAG,"url=="+url);
//        LogUtils.Logd(TAG,"params=="+params);
        CommonOkHttpClient.get(CommonRequest.
                createPostRequest(url, params,requestParams), new DisposeDataHandle(listener, clazz));
    }


}
