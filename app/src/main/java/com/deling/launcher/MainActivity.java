package com.deling.launcher;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Instrumentation;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.deling.launcher.application.MyApplication;
import com.deling.lib_common_ui.base.BaseMainActivity;
import com.wanxiang.commonlib.CommonLib;
import com.wanxiang.commonlib.utils.ApkUtils;
import com.wanxiang.commonlib.utils.LogUtils;
import com.wanxiang.commonlib.utils.PropertiesUtils;
import com.wanxiang.commonlib.utils.ThreadPoolUtils;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
public class MainActivity extends BaseMainActivity implements // BottomAppAdapter.OnItemClickLitener,
        View.OnClickListener {

/*    private Context mContext;
    private RelativeLayout main_title_layout;
     ImageView mainBluetoothIcon;*/
//    ImageView mainWifiIcon;
//    TextView batteryText;
    ImageView    education, videoDoctor,tocall;  //heath_check, video_doctor;
    ConstraintLayout batteryLayout;
    public static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    public static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";//按下home键
    private static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";

    private static final String TAG = "MainActivity";


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().getDecorView().setBackground(null);
        setContentView(R.layout.activity_main);
        education=findViewById(R.id.education);
        videoDoctor=findViewById(R.id.video_doctor);
        tocall=findViewById(R.id.tocall);
        videoDoctor.setOnClickListener(this);
        education.setOnClickListener(this);
        tocall.setOnClickListener(this);
        stopAnimation();
    /*    isFirstInit = true;
        mContext = CommonLib.getContext();*/
        initData();
        initBroadCast();
     }

    @Override
    protected void onResume() {
        super.onResume();
     }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initBroadCast() {
        registerWifiReceiver();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        LogUtils.Logd(TAG, "onTouchEvent: ");
        return super.onTouchEvent(event);
    }

    private void initData() {
        LogUtils.d("MainActivity  initData");
    }

    @Override
    protected void onPause() {
        super.onPause();
 //        DebugUtils.printTime(TAG, "onPause end");
    }

    @Override
    protected void onStop() {
        super.onStop();
     }





    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.education){
           LogUtils.Logd(TAG,"  heath_check  精品教育");

            PackageManager packageManager = CommonLib.getContext().getPackageManager();
            Intent intent=packageManager.getLaunchIntentForPackage("com.wyt.wangkexueximvvm");
            if(intent==null){
                Toast.makeText(MyApplication.getmContext(), "未安装该应用", Toast.LENGTH_SHORT).show();
            }else{
                CommonLib.getContext().startActivity(intent);
            }
         //  AppUtils.launchApp("com.baikang");
        }else if(v.getId()==R.id.video_doctor){
            LogUtils.Logd(TAG,"  com.hhmedic.app.patient  视频医生");
            //startActivity(new Intent(MainActivity.this,VideoDoctorActivity.class));

            PackageManager packageManager = CommonLib.getContext().getPackageManager();
            Intent intent=packageManager.getLaunchIntentForPackage("com.hhmedic.app.patient");
            if(intent==null){
                Toast.makeText(MyApplication.getmContext(), "未安装该应用", Toast.LENGTH_SHORT).show();
            }else{
                CommonLib.getContext().startActivity(intent);
            }

        }else if(v.getId()==R.id.tocall){
            LogUtils.Logd(TAG,"  tocall");
            //AppUtils.launchApp("com.x.srihomePlat");
            PackageManager packageManager = CommonLib.getContext().getPackageManager();
         //   Intent intent=packageManager.getLaunchIntentForPackage("com.yunzhixun.yzx_drobot");
            Intent intent=packageManager.getLaunchIntentForPackage("com.x.srihomePlat");
            if(intent==null){
                Toast.makeText(MyApplication.getmContext(), "未安装该应用", Toast.LENGTH_SHORT).show();
            }else{
                CommonLib.getContext().startActivity(intent);
            }

        }
     }


    @SuppressLint("RestrictedApi")
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            return true;
        } else
            return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterWifiReceiver();
    }



    private void registerWifiReceiver() {
        if (wifiOrBluetoothReceiver != null) {
            return;
        }
        wifiOrBluetoothReceiver = new WifiOrBluetoothReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(wifiOrBluetoothReceiver, filter);
    }

    private void unRegisterWifiReceiver() {
        if (wifiOrBluetoothReceiver == null) {
            return;
        }
        unregisterReceiver(wifiOrBluetoothReceiver);
        wifiOrBluetoothReceiver = null;
    }


    private WifiOrBluetoothReceiver wifiOrBluetoothReceiver;
    private class WifiOrBluetoothReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
          LogUtils.Logd(TAG,"action == " + action);
            if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                int wifistate = intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_STATE,
                        WifiManager.WIFI_STATE_DISABLED);

                if (wifistate == WifiManager.WIFI_STATE_DISABLED) {
                //    mainWifiIcon.setVisibility(View.INVISIBLE);
                } else if (wifistate == WifiManager.WIFI_STATE_ENABLED) {
                    if (NetworkUtils.isConnected()) {
                        LogUtils.Logd(TAG, "  Wifi 可用了!");
                        updateWifiStrength();
                    }
                }
            } else if (action.equals(WifiManager.RSSI_CHANGED_ACTION)) {
                if (NetworkUtils.isConnected()) {
                    initWifiState();
                }
            } else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                if (!NetworkUtils.isConnected()) {
                //    mainWifiIcon.setVisibility(View.INVISIBLE);
                } else {
                 //   mainWifiIcon.setVisibility(View.VISIBLE);
                }
            }

            if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
              //  mainBluetoothIcon.setVisibility(View.INVISIBLE);
              LogUtils.Logd(TAG,"蓝牙设备已断开");

            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
              //  mainBluetoothIcon.setVisibility(View.VISIBLE);

              LogUtils.Logd(TAG,"蓝牙设备已连接");
            }

            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);

              LogUtils.Logd(TAG,"reason==" + reason);
                if (SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) { // 短按Home键
                  LogUtils.Logd(TAG,"t8sbc短按Home键");
                 } else if (SYSTEM_DIALOG_REASON_RECENT_APPS.equals(reason)) {//按了切换键
                 }
            }
        }
    }

    private int getStrength(Context context) {
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        if (info.getBSSID() != null) {
            int strength = WifiManager.calculateSignalLevel(info.getRssi(), 5);
            return strength;
        }
        return 0;
    }

    private void initWifiState() {
        updateWifiStrength();
    }

    private void updateWifiStrength() {
      /*  mainWifiIcon.setVisibility(View.VISIBLE);
        int strength = getStrength(this);
        if (strength > 0) {
            //mainWifiIcon.setImageResource(wifiStateImgs[strength - 1]);
        } else {
          //  mainWifiIcon.setImageResource(R.drawable.wifi_1);
        }*/
    }

/*    private int[] wifiStateImgs = new int[]{R.drawable.wifi_1,
            R.drawable.wifi_2, R.drawable.wifi_3,
            R.drawable.wifi_4};*/


    @Override
    public void finish() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
        overridePendingTransition(0, 0);
    }

    private void stopAnimation() {
        CommonLib.getAsynHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                PropertiesUtils.set("service.bootanim.exit", "1");
            }
        }, 38 * 1000);
    }

}

